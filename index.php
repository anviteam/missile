<!DOCTYPE html>
<html>
<head>
<title>Missile trajectory</title>

<link rel="stylesheet" type="text/css" href="js/vendor/closure/goog/css/toolbar.css">
<link rel="stylesheet" type="text/css" href="js/vendor/closure/goog/css/menu.css">
<link rel="stylesheet" type="text/css" href="js/vendor/closure/goog/css/menubar.css">
<link rel="stylesheet" type="text/css" href="js/vendor/closure/goog/css/submenu.css">
<!--<script src="/closure/closure-library-master/closure/goog/base.js"></script>-->
<script src="js/vendor/closure/goog/base.js"></script>

<link rel="stylesheet" type="text/css" href="js/vendor/ol/ol.css">
<script src="js/vendor/ol/ol.js"></script>

<link rel="stylesheet" type="text/css" href="css/pure-min.css">

<link rel="stylesheet" type="text/css" href="css/sweetalert.css">
<link rel="stylesheet" type="text/css" href="js/vendor/anvi/src/css/anvi.css">
<link rel="stylesheet" type="text/css" href="css/main.css">

</head>

<body onload="init()">
	<div id="map"></div>

    <script src="js/vendor/anvi/anvi-debug.js"></script>
    <script src="js/src/main.js"></script>

    <!-- Import form -->
    <div id="import_widget">
    <div class="closeSymbol" onclick="document.getElementById('import_widget').style.display = 'none';">x</div>
    <form class="pure-form pure-form-stacked" id="import_form">
    <fieldset>
        <legend>File data import</legend>

        <label for="name">Data files</label>
        <input id="uploadFile" placeholder="Choose File" disabled="disabled" class="pure-input-2-3"/>
        <div class="fileUpload pure-button pure-button-primary">
            <span>Browser</span>
            <input id="uploadFileInput" type="file" class="upload" multiple />
        </div>

        <label for="type_file_select">Data type</label>
        <select class="pure-select" id="type_file_select">
            <option value="plot:height">Plot height</option>
            <option value="plot:surface">Plot surface</option>
            <option value="plot:ecmwf_height">Plot ECmwf height</option>
            <option value="plot:ecmwf_wind">Plot ECmwf wind</option>
            <option value="observation:height">Height</option>
            <option value="observation:temperature">Temperature</option>
            <option value="plot:rain">Rain</option>
            <option value="observation:wind">Wind</option>
            <option value="radar">Radar</option>
            <option value="satellite">Satellite</option>
            <option value="missile">Missile</option>
            <option value="ec:height">EC height</option>
            <option value="ec:wind">EC wind</option>
            <option value="ec:thin">EC thin</option>
        </select>

        <button type="submit" id="import_form_submit" class="pure-button pure-button-primary">Submit</button>
    </fieldset>
    </form>
    </div>

    <!-- Export form -->
    <div id="export_widget">
        <div class="closeSymbol" onclick="document.getElementById('export_widget').style.display = 'none';">x</div>
        <form class="pure-form pure-form-aligned" id="import_form">
        <fieldset>
            <label for="name">Export formats</label>
            <input id="export-kmz" type="radio" name="export-radio"/>
            <input id="export-csv" type="radio" name="export-radio"/>
            <div class="pure-controls">
                <button type="submit" id="import_form_submit" class="pure-button pure-button-primary">Submit</button>
            </div>
        </fieldset>
        </form>
    </div>

	<div id="loading">
        <img  src="css/images/loading.gif" />
        <!-- 
        <div style="margin-top: 10px; color: white">
            <b>Please wait</b>
        </div>
    	-->
    </div>

    <script type="text/javascript">
        document.getElementById("uploadFileInput").onchange = function (e) {
            var fpath = e.target.files[0].name;
            document.getElementById("uploadFile").value = fpath;
        };
    </script>

    <script src="js/vendor/FileSaver.min.js"></script>
    <script src="js/vendor/XMLWriter-1.0.0.js"></script>
    <script src="js/src/sweetalert.min.js"></script>
</body>
</html>