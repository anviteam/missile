--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


SET search_path = public, pg_catalog;

--
-- Name: ec_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ec_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ec_rid_seq OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ecmwf; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ecmwf (
    rid integer DEFAULT nextval('ec_rid_seq'::regclass) NOT NULL,
    rast raster,
    datetime timestamp without time zone,
    pressure numeric,
    variable character varying(32)
);


ALTER TABLE ecmwf OWNER TO postgres;

--
-- Name: missile_path; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE missile_path (
    id integer NOT NULL,
    status smallint,
    lon real,
    lat real,
    height real,
    datetime timestamp without time zone
);


ALTER TABLE missile_path OWNER TO postgres;

--
-- Name: missile_trajectory_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE missile_trajectory_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE missile_trajectory_id_seq OWNER TO postgres;

--
-- Name: missile_trajectory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE missile_trajectory_id_seq OWNED BY missile_path.id;


--
-- Name: observation; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE observation (
    rid integer NOT NULL,
    rast raster,
    datetime timestamp without time zone,
    pressure numeric,
    variable character varying(32)
);


ALTER TABLE observation OWNER TO postgres;

--
-- Name: observation_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE observation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE observation_id_seq OWNER TO postgres;

--
-- Name: observation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE observation_id_seq OWNED BY observation.rid;


--
-- Name: plot_ecmwf_height; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE plot_ecmwf_height (
    id integer NOT NULL,
    staid bigint,
    value numeric,
    geom geometry,
    datetime timestamp without time zone,
    pressure numeric
);


ALTER TABLE plot_ecmwf_height OWNER TO postgres;

--
-- Name: plot_ecmwf_height_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE plot_ecmwf_height_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE plot_ecmwf_height_id_seq OWNER TO postgres;

--
-- Name: plot_ecmwf_height_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE plot_ecmwf_height_id_seq OWNED BY plot_ecmwf_height.id;


--
-- Name: plot_ecmwf_wind_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE plot_ecmwf_wind_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE plot_ecmwf_wind_id_seq OWNER TO postgres;

--
-- Name: plot_ecmwf_wind; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE plot_ecmwf_wind (
    id integer DEFAULT nextval('plot_ecmwf_wind_id_seq'::regclass) NOT NULL,
    staid bigint,
    height numeric,
    t numeric,
    delta_dew numeric,
    wd numeric,
    ws numeric,
    datetime timestamp without time zone,
    geom geometry,
    pressure numeric
);


ALTER TABLE plot_ecmwf_wind OWNER TO postgres;

--
-- Name: plot_height; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE plot_height (
    id integer NOT NULL,
    staid bigint,
    height numeric,
    t numeric,
    delta_dew numeric,
    wd numeric,
    ws numeric,
    datetime timestamp without time zone,
    geom geometry,
    pressure numeric
);


ALTER TABLE plot_height OWNER TO postgres;

--
-- Name: plot_height_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE plot_height_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE plot_height_id_seq OWNER TO postgres;

--
-- Name: plot_height_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE plot_height_id_seq OWNED BY plot_height.id;


--
-- Name: plot_surface; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE plot_surface (
    id integer NOT NULL,
    staid bigint,
    wd numeric,
    ws numeric,
    p numeric,
    dp3 numeric,
    prep numeric,
    dew numeric,
    visibility numeric,
    t numeric,
    dt numeric,
    dp24 numeric,
    geom geometry,
    datetime timestamp without time zone
);


ALTER TABLE plot_surface OWNER TO postgres;

--
-- Name: plot_surface_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE plot_surface_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE plot_surface_id_seq OWNER TO postgres;

--
-- Name: plot_surface_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE plot_surface_id_seq OWNED BY plot_surface.id;


--
-- Name: radar; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE radar (
    rid integer NOT NULL,
    datetime timestamp without time zone,
    lat numeric,
    lon numeric,
    data bytea,
    xres numeric,
    yres numeric,
    imgsize character(16)
);


ALTER TABLE radar OWNER TO postgres;

--
-- Name: radar_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE radar_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE radar_rid_seq OWNER TO postgres;

--
-- Name: radar_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE radar_rid_seq OWNED BY radar.rid;


--
-- Name: rain; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE rain (
    id integer NOT NULL,
    staid bigint,
    value numeric,
    geom geometry,
    datetime timestamp without time zone
);


ALTER TABLE rain OWNER TO postgres;

--
-- Name: rain_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE rain_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE rain_id_seq OWNER TO postgres;

--
-- Name: rain_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE rain_id_seq OWNED BY rain.id;


--
-- Name: satellite_rid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE satellite_rid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE satellite_rid_seq OWNER TO postgres;

--
-- Name: satellite; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE satellite (
    rid integer DEFAULT nextval('satellite_rid_seq'::regclass) NOT NULL,
    datetime timestamp without time zone,
    lat numeric,
    lon numeric,
    xres numeric,
    yres numeric,
    data bytea,
    imgsize character(16)
);


ALTER TABLE satellite OWNER TO postgres;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY missile_path ALTER COLUMN id SET DEFAULT nextval('missile_trajectory_id_seq'::regclass);


--
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY observation ALTER COLUMN rid SET DEFAULT nextval('observation_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY plot_ecmwf_height ALTER COLUMN id SET DEFAULT nextval('plot_ecmwf_height_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY plot_height ALTER COLUMN id SET DEFAULT nextval('plot_height_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY plot_surface ALTER COLUMN id SET DEFAULT nextval('plot_surface_id_seq'::regclass);


--
-- Name: rid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY radar ALTER COLUMN rid SET DEFAULT nextval('radar_rid_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY rain ALTER COLUMN id SET DEFAULT nextval('rain_id_seq'::regclass);


--
-- Name: ec_data_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ecmwf
    ADD CONSTRAINT ec_data_pkey PRIMARY KEY (rid);


--
-- Name: ecmwf_time_sequence_pressure_variable_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ecmwf
    ADD CONSTRAINT ecmwf_time_sequence_pressure_variable_key UNIQUE (datetime, pressure, variable);


--
-- Name: missile_trajectory_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY missile_path
    ADD CONSTRAINT missile_trajectory_pkey PRIMARY KEY (id);


--
-- Name: observation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY observation
    ADD CONSTRAINT observation_pkey PRIMARY KEY (rid);


--
-- Name: observation_pressure_time_sequence_variable_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY observation
    ADD CONSTRAINT observation_pressure_time_sequence_variable_key UNIQUE (pressure, datetime, variable);


--
-- Name: observation_rid_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY observation
    ADD CONSTRAINT observation_rid_key UNIQUE (rid);


--
-- Name: plot_ecmwf_height_staid_geom_datetime_pressure_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY plot_ecmwf_height
    ADD CONSTRAINT plot_ecmwf_height_staid_geom_datetime_pressure_key UNIQUE (staid, geom, datetime, pressure);


--
-- Name: plot_ecmwf_wind_staid_datetime_geom_pressure_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY plot_ecmwf_wind
    ADD CONSTRAINT plot_ecmwf_wind_staid_datetime_geom_pressure_key UNIQUE (staid, datetime, geom, pressure);


--
-- Name: plot_height_staid_datetime_geom_layer_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY plot_height
    ADD CONSTRAINT plot_height_staid_datetime_geom_layer_key UNIQUE (staid, datetime, geom, pressure);


--
-- Name: plot_surface_staid_geom_datetime_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY plot_surface
    ADD CONSTRAINT plot_surface_staid_geom_datetime_key UNIQUE (staid, geom, datetime);


--
-- Name: radar_time_sequence_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY radar
    ADD CONSTRAINT radar_time_sequence_key UNIQUE (datetime);


--
-- Name: rain_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY rain
    ADD CONSTRAINT rain_id_key UNIQUE (id);


--
-- Name: rain_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY rain
    ADD CONSTRAINT rain_pkey PRIMARY KEY (id);


--
-- Name: rain_staid_datetime_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY rain
    ADD CONSTRAINT rain_staid_datetime_key UNIQUE (staid, datetime);


--
-- Name: satellite_time_sequence_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY satellite
    ADD CONSTRAINT satellite_time_sequence_key UNIQUE (datetime);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

