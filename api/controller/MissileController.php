<?php

namespace Anvi\Controller;

use Anvi\Model\MissilePath;
use Anvi\Helper\IO;

/**
 * Missille controller
 */

class MissileController {
	
	function getPath($request, $response, $args) {
        $resp = $response->withHeader('Content-type', 'application/json')
        	->withStatus(200);

    	// Fetch all
    	//$missile = MissilePath::where('datetime','=',$args['datetime'])
    	//					   ->select('datetime')->get();
    	//$traj = $missile->toJson();
    	$traj = MissilePath::toWKT($args['datetime']);

		// JsonResponse
	    $resp->getBody()->write(json_encode($traj));


	    return $resp;
	}

	function getAllPaths($request, $response, $args) {
		$resp = $response->withHeader('Content-type', 'application/json')->withStatus(200);

    	// Fetch all
        try {
            $sql = "SELECT DISTINCT datetime FROM missile_path";

            $q = \ORM::get_db()->prepare($sql);
            if ($q->execute()) {
                $data = $q->fetchAll(\PDO::FETCH_COLUMN);
            }
        } catch (\PDOException $e) {
            throw $e;
        }

		// JsonResponse
	    $resp->getBody()->write(json_encode($data));

	    return $resp;
	}

	function set($request, $response, $args) {
        try {
            $resp = $response->withHeader('Content-type', 'application/json');
            $ret = array('message'=>"No content", 'error' => 0);

            if (!empty($_FILES)) {
            	$content = file_get_contents($_FILES['missile']['tmp_name']);
            } else if (!empty($_POST) && isset($_POST['missile'])) {
            	$content = $_POST['missile'];
            } else if ($request->getContentLength()>0) {
                $content = $request->getBody()->getContents();
            }

            if (isset($content)) {
            	$dataLines = IO::readMissileData ($content);
                $ret['message'] = "$dataLines points have been inserted";
            }

            $resp->getBody()->write(json_encode($ret));

            return $resp;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }
}