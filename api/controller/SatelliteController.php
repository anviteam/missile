<?php
/**
 * Satellite controller
 */ 

namespace Anvi\Controller;

use GuzzleHttp\Psr7;
use Anvi\Model\Satellite;

class SatelliteController {
	/**
	 * Get the radar meta data
	 */
	public function getMeta($request, $response, $args) {
		// Get data from request
		$uri = $request->getUri()->__toString();
		$dt = urldecode($args['datetime']);

        // Response
		$resp = $response->withStatus(200)
	        ->withHeader('Content-Type', 'application/json');
	    $resp->getBody()->write(json_encode(array(
	    	"meta" => Satellite::getMeta($dt),
	    	"imgUrl" => $uri.'/png'
	    	))
	    );

    	return $resp;
	}

	/**
	 * Get the radar data as image
	 */
	public function getImage($request, $response, $args) {
		$dt = urldecode($args['datetime']);
		// Get the Satellite Model
		$image = Satellite::toByteImage($dt);
		$stream = Psr7\stream_for($image);
        // Response
		return $response->withStatus(200)
	        ->withHeader('Content-Type', 'image/png')
	        ->withHeader('Content-Length', $stream->getSize())
	        ->withBody($stream);
	}

	public function setData($request, $response, $args) {
		
		$ret = array('message'=>"No content", 'error' => 0);

		/*MapTiler ex
		$map_tiler = new \Anvi\Helper\MapTiler\MapTiler('C:\wamp\www\missile\app\awx.png', array(
		  'tiles_path' => './data/',
		  'zoom_max' => 8,
		  'format' => 'png'
		));
		//execute
		try {
		  $map_tiler->process(true);
		} catch (Exception $e) {
		  echo $e->getMessage();
		  echo $e->getTraceAsString();
		}//*/

		try {
			if(empty($_FILES)) {
				$ret['error'] = "No file found!";
			} else if(!array_key_exists('satellite', $_FILES)) {
				$ret['error'] = "No 'satellite' key found!";
			} else  if ( is_uploaded_file($_FILES['satellite']['tmp_name']) ) {
				$awxFname = 'runtime'.DIRECTORY_SEPARATOR.'file.satellite';

			    move_uploaded_file($_FILES['satellite']['tmp_name'], $awxFname);

			    // Run ReadRadar runtime
			    $prog = 'runtime'.DIRECTORY_SEPARATOR.'ReadAWX.exe';
			    $cmd = "{$prog} {$awxFname}";
			    exec($cmd, $output);

			    // Save the output into db
			    if(!empty($output)) {
			    	$table = "satellite";
			    	$imgwid = $output[0];$imghei = $output[1];
					$lat = 0.01*$output[2];
					$lon = 0.01*$output[3];
					$xres = 1000*$output[4]; // m/pixel
					$yres = 1000*$output[5]; // m/pixel
					$datetime = $output[6];
					$imgsize = $output[7];
					$base64 = $output[8];

					// Add new data
					$sql =  "
						INSERT INTO {$table} (data,lat,lon,datetime,xres,yres,imgsize)
						VALUES (
							decode('{$base64}', 'base64'),
							{$lat},{$lon},
							'{$datetime}',
							{$xres},{$yres},'{$imgsize}')
						";
					$db = \ORM::get_db();
					$out = $db->exec($sql);
					if ($out) {
						$ret['message'] = "Successful";
					}
			    } else {
			    	$ret["message"] = "ReadRadar runtime has released no output";
			    	$ret["error"] = 1;
			    }
			    
			}
			
		} catch (\Exception $ex) {
			$ret["error"] = $ex->getCode();
			$ret["message"] = $ex->getMessage();
		}//*/

		// Response
		$resp = $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(201);

		// JsonResponse
	    $resp->getBody()->write(json_encode($ret));

	    return $resp;
	}
}