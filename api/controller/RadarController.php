<?php

/**
 * Radar controller
 */ 

namespace Anvi\Controller;

use GuzzleHttp\Psr7;
use Anvi\Model\Radar;

class RadarController {
	/**
	 * Get the radar meta data
	 */
	public function getMeta($request, $response, $args) {
		// Get data from request
		$uri = $request->getUri()->__toString();
		$dt = urldecode($args['datetime']);

        // Response
		$resp = $response->withStatus(200)
	        ->withHeader('Content-Type', 'application/json');

	    $resp->getBody()->write(json_encode(array(
	    	"meta" => Radar::getMeta($dt),
	    	"imgUrl" => $uri.'/png'
	    	))
	    );

    	return $resp;
	}

	/**
	 * Get the radar data as image
	 */
	public function getImage($request, $response, $args) {
		$dt = urldecode($args['datetime']);

		// Get the Radar as image
		$image = \Anvi\Model\Radar::toByteImage($dt);
		$stream = Psr7\stream_for($image);
        // Response
		return $response->withStatus(200)
	        ->withHeader('Content-Type', 'image/png')
	        ->withHeader('Content-Length', $stream->getSize())
	        ->withBody($stream);
	}

	public function setData($request, $response, $args) {
		
		$ret = array('message'=>"No content", 'error' => 0);

		try {
			if(empty($_FILES)) {
				$ret['error'] = "No file found!";
			} else if(!array_key_exists('radar', $_FILES)) {
				$ret['error'] = "No 'radar' key found!";
			} else  if ( is_uploaded_file($_FILES['radar']['tmp_name']) ) {
				$radarFname = 'runtime'.DIRECTORY_SEPARATOR.'file.radar';

			    move_uploaded_file($_FILES['radar']['tmp_name'], $radarFname);

			    // Run ReadRadar runtime
			    $paletteFname = 'runtime'.DIRECTORY_SEPARATOR.'palette.pal';
			    $prog = 'runtime'.DIRECTORY_SEPARATOR.'ReadRadar.exe';
			    $cmd = "{$prog} {$radarFname} {$paletteFname}";
			    //$output = shell_exec($cmd);
			    exec($cmd, $output);

			    // Save the output into db
			    if(!empty($output)) {					
					$lat = 0.001*$output[0];
					$lon = 0.001*$output[1];
					$datetime = $output[2];
					$imgsize = $output[3];
					$base64 = $output[4];

					$xres = 1000; // m
					$yres = 1000; // m

					$sql = "
						INSERT INTO radar (data,lat,lon,datetime,imgsize,xres,yres)
						VALUES (
							decode('{$base64}', 'base64'),
							{$lat},{$lon},
							'{$datetime}','{$imgsize}',{$xres},{$yres})
						";
					$db = \ORM::get_db();
					$out = $db->exec($sql);
					if ($out) {
						$ret["message"] = "Successful";
					}
			    } else {
			    	$ret["message"] = "ReadRadar runtime has released no output";
			    	$ret["error"] = 1;
			    }
			    
			}
		} catch (\Exception $ex) {
			$ret["error"] = $ex->getCode();
			$ret["message"] = $ex->getMessage();
		}

		// Response
		$resp = $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(201);

		// JsonResponse
	    $resp->getBody()->write(json_encode($ret));

	    return $resp;
	}

	public function putData ($request, $response, $args) {
		$ret = array('message'=>"No content", 'error' => 0);

		try {
			

		} catch (\Exception $ex) {
			$ret["error"] = $ex->getMessage();
		}

		// Response
		$newResponse = $response->withHeader('Content-type', 'application/json');

		// JsonResponse
	    $newResponse->getBody()->write(json_encode($ret));

	    return $newResponse;
	}
}