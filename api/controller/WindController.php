<?php

namespace Anvi\Controller;

use Anvi\Model\ECWind;
use Anvi\Helper\IO;

class WindController {
	
	public function getWindVelocity($request, $response, $args) {
		$newResponse = $response
			->withHeader('Content-type', 'application/json')
			->withStatus(200);

		// Get the request query
        if(empty($args['datetime'])) {
            throw new \Exception("datetime parameter was not found! [/{datetime}]");
        }
        $args['datetime'] = urldecode($args['datetime']);

        if(!isset($args['pressure'])) {
            $args['pressure'] = 9999;
        }

        // ECWind uses by default "observation" as table
		//$wdata = ECWind::getData($args['variable'],$args['datetime'],$args['pressure'],1);

		// ECData uses 'ecmwf' as table
		$wdata = IO::getECData($args['variable'], $args['datetime'], floatval($args['pressure']));
		if (empty($wdata)) {
			$newResponse->getBody()->write(json_encode(array(
				'message'=>'No data or some values are NULL in the data',
				'error'=>1
				)));
		} else {
			// JsonResponse
		    $newResponse->getBody()->write(json_encode($wdata));
		}

	    return $newResponse;
	}

	public function setData($request, $response, $args) {
		try {
			$resp = $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(201);
            $ret = array('message'=>"No content", 'error' => 0);

            if (!empty($_FILES)) {
                $content = file_get_contents($_FILES['ec']['tmp_name']);
            } else if (!empty($_POST) && isset($_POST['ec'])) {
                $content = $_POST['ec'];
            } else if ($request->getContentLength()>0) {
                $content = $request->getBody()->getContents();
            }
            
            // Read data from $request body
            if ($content) {
                // Call db function
                $ret["message"] = IO::putEcmwfData($content,$args['variable'])." records have been inserted";
            }
			
			// Read the request content type
			//$contentType = $request->getContentType();
			//$ret["Content-type"] = $request->getHeader('Accept');

			// JsonResponse
            $resp->getBody()->write(json_encode($ret));

		} catch (\Exception $ex) {
			$ret["message"] = $ex->getMessage();
		}

	    return $resp;
	}
}