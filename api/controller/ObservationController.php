<?php
// Ground data

namespace Anvi\Controller;

use Anvi\Model\ObservationWeather;
use Anvi\Model\ECWind;
use Anvi\Helper\IO;

class ObservationController {

	public function get($request, $response, $args) {
        /*
        if($args['format'] === "kml") {
            $resp = $response->withHeader('Content-type', 'application/vnd.google-earth.kml+xml');
            try {
                //$kml = new KML('Ground data of '.$args['variable']);
                //$document = new KMLDocument('tetris', 'TETRIS');

            } catch (\Exception $ex) {
                throw $ex;
            }

        } else {*/
            $resp = $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(200);
            $ret = array('message'=>"No content", 'error' => 0);

            try {
                if(empty($args['datetime'])) {
                    throw new \Exception("datetime parameter was not found! [/{datetime}]");
                }
                $args['datetime'] = urldecode($args['datetime']);

                if(!isset($args['pressure'])) {
                    $args['pressure'] = 9999;
                }

                if ($args['variable'] !== "wind") {
                    $gWeaObj = new ObservationWeather(
                        $args['variable'], $args['datetime'], floatval($args['pressure']));
                    $contours = $gWeaObj->toGeoJson();
                    // JsonResponse
                    $resp->getBody()->write(json_encode(
                        array("contour" => $contours,"stats" => $gWeaObj->getStats())
                        ));//*/
                } else {
                    $gWeaObj = ECWind::getData($args['variable'],$args['datetime'],$args['pressure'],1);
                    $resp->getBody()->write(json_encode($gWeaObj));
                }

            } catch (\Exception $ex) {
                throw $ex;
            }
        //}

        return $resp;
    }

    public function set($request, $response, $args) {
        try {
            $resp = $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(201);
            $ret = array('message'=>"No content", 'error' => 0);

            if (!empty($_FILES)) {
                $content = file_get_contents($_FILES['observation']['tmp_name']);
            } else if (!empty($_POST) && isset($_POST['observation'])) {
                $content = $_POST['observation'];
            } else if ($request->getContentLength()>0) {
                $content = $request->getBody()->getContents();
            }
            
            // Read data from $request body
            if ($content) {
                // Call db function
                $ret["message"] = IO::putObservationData($content,$args['variable'])." records have been inserted";
            }

            // JsonResponse
            $resp->getBody()->write(json_encode($ret));
        } catch (\Exception $ex) {
            throw $ex;
        }

        return $resp;
    }
}