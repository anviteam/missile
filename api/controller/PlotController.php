<?php

namespace Anvi\Controller;

use Anvi\Helper\IO;

class PlotController {

	function get($request, $response, $args) {
        try {
            $resp = $response->withHeader('Content-type', 'application/json');
            $ret = array('message'=>"No content", 'error' => 0);

            if(empty($args['datetime'])) {
                throw new \Exception("datetime parameter was not found! [/{datetime}]");
            }
            $args['datetime'] = urldecode($args['datetime']);

            if(!isset($args['pressure'])) {
                $args['pressure'] = null;
            }

        	$ret = IO::getPlotData (
                $args['variable'],
                urldecode($args['datetime']),$args['pressure']);

            $resp->getBody()->write(json_encode($ret));

            return $resp;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

	function set($request, $response, $args) {
        try {
            $resp = $response->withHeader('Content-type', 'application/json');
            $ret = array('message'=>"No content", 'error' => 0);

            if (!empty($_FILES)) {
            	$content = file_get_contents($_FILES['plot']['tmp_name']);
            } else if (!empty($_POST) && isset($_POST['plot'])) {
            	$content = $_POST['plot'];
            } else if ($request->getContentLength()>0) {
                $content = $request->getBody()->getContents();
            }

            if (isset($content)) {
            	$dataLines = IO::putPlotData ($content, $args['variable']);
                $ret['message'] = "{$dataLines} records have been inserted";
            }

            $resp->getBody()->write(json_encode($ret));

            return $resp;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }
}