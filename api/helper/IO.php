<?php

namespace Anvi\Helper;

use GeoJson\GeoJson;
use GeoJson\Geometry\Point;
use GeoJson\Feature\Feature;
use GeoJson\Feature\FeatureCollection;

class IO {

	static function getWindDir ($u, $v) {
    	$r2d = (180./M_PI);

    	if($u < 0) {
    		if ($v != 0) {
    			$a = 90. - atan($v/$u)*$r2d;
    		} else {
    			$a = 90.;
    		}
    	} else if ($u > 0) {
    		if ($v != 0) {
    			$a = 270. - atan($v/$u)*$r2d;
    		} else {
    			$a = 270.;
    		}
    	} else {
    		if ($v > 0) {
    			$a = 180.;
    		} else if ($v < 0) {
    			$a = 360.;
    		} else {
    			$a = 0;
    		}
    	}

    	return $a;
    }

	static function getTimeSequences () {
		// Get all table names
		$sql = "
			SELECT table_name
  			FROM information_schema.tables
 			WHERE table_schema='public'
   			AND table_type='BASE TABLE';
		";
		
		// Query
		try {
			// Get all table names
			$q = \ORM::get_db()->prepare($sql);
			if ( $q->execute() ) {
				$tables = $q->fetchAll(\PDO::FETCH_COLUMN);
			}

			$data = array();
			foreach ($tables as $table) {
				$sql = "
					SELECT DISTINCT datetime
		  			FROM {$table}
				";
				try {
					// Get time sequences
					$q = \ORM::get_db()->prepare($sql);
					if ( $q->execute() ) {
						$data = array_merge($data, $q->fetchAll(\PDO::FETCH_COLUMN));
					}
				} catch (\PDOException $e) {
					continue;
				}
			}

			// Remove duplicate elements
			$data = array_unique($data);

			// Sort the data array
			usort($data, function($a, $b) {
			  	$ad = new \DateTime($a);
			  	$bd = new \DateTime($b);

			  	if ($ad == $bd) {
			    	return 0;
			  	}

			  	return $ad < $bd ? -1 : 1;
			});
			
		} catch (\PDOException $ex) {
			throw $ex;
		}

		return (isset($data)) ? $data : null;
	}

	/**
	 * Get the Observaton weather data points from content to DB
	 */
	static function getObservationData($table, $variable, $timeseq, $pressure, $nband = 1) {

		// SQL
		$sql =  "
			SELECT (ST_DumpValues(rast, {$nband})) as valarray
			FROM {$table}
			WHERE variable='{$variable}' AND datetime = '{$timeseq}' AND pressure = {$pressure}
		";
	
		//echo $sql . '<br>';

		try {
			$q = \ORM::get_db()->prepare($sql);
			if ( $q->execute() ) {
				$vals = $q->fetch(\PDO::FETCH_ASSOC)["valarray"];
				$vals = preg_replace('/{/','[',$vals);
			 	$vals = preg_replace('/}/',']',$vals);
			 	$data = json_decode($vals);
				/*
				$data = array();
				while ( $row = $q->fetch(\PDO::FETCH_ASSOC) ) {
					$vals = $row["valarray"];
				 	$vals = preg_replace('/{/','[',$vals);
				 	$vals = preg_replace('/}/',']',$vals);
				 	$data[] = json_decode($vals);
				}*/
			}
		} catch (\PDOException $ex) {
			throw $ex;
		}

		// Output
		if(isset($data)) {
			//$data = preg_replace('/{/','[',$data);
			//$data = preg_replace('/}/',']',$data);
			return ($data);
		}

		return null;
	}

	/**
	 * Get the EC model weather data points from content to DB
	 */
	static function getECData ($variable,$datetime,$pressure) {
		$db = \ORM::get_db();
    	$table = "ecmwf";
    	if ($variable == "wind" || $variable == "thin") {
	    	$sql ="
	    		WITH foo_u as (
					SELECT (ST_PixelAsCentroids(rast, 1)).* FROM {$table}
					WHERE variable='{$variable}' AND datetime='{$datetime}' AND pressure={$pressure}
				),
				foo_v as (
					SELECT (ST_PixelAsCentroids(rast, 2)).* FROM {$table}
					WHERE variable='{$variable}' AND datetime='{$datetime}' AND pressure={$pressure}
				)
				SELECT foo_u.x as i, foo_u.y as j, foo_u.val as u, foo_v.val as v, ST_X(foo_u.geom) as lon, ST_Y(foo_u.geom) as lat
				FROM foo_u JOIN foo_v ON foo_u.x = foo_v.x AND foo_u.y = foo_v.y
	    	";
	    } else if ($variable === "height") {
	    	$sql =  "
				SELECT (ST_DumpValues(rast, 1)) as valarray
				FROM {$table}
				WHERE variable='{$variable}' AND datetime = '{$datetime}' AND pressure = {$pressure}
			";
	    } else {
	    	throw new \Exception("$variable is not supported.");
	    }

	    if ($variable === "height") {
	    	try {
				$q = $db->prepare($sql);
				if ( $q->execute() ) {
					$vals = $q->fetch(\PDO::FETCH_ASSOC)["valarray"];
					$vals = preg_replace('/{/','[',$vals);
				 	$vals = preg_replace('/}/',']',$vals);
				 	$data = json_decode($vals);
				}
			} catch (\PDOException $ex) {
				throw $ex;
			}

			// Output
			if(isset($data)) {
				return ($data);
			}

			return null;

	    } else if ($variable == "wind" || $variable == "thin") {
	    	$res = array();
	    	$skip = 1;
	    	try {
				$q = $db->prepare($sql);
				if ( $q->execute() ) {
					while ( $row = $q->fetch(\PDO::FETCH_ASSOC) ) {
						array_push($res,$row);
					}
				}
			} catch (\PDOException $ex) {
				throw $ex;
			}
	//var_dump(memory_get_peak_usage(true));
			// Output
			if(!empty($res)) {
				// Convert to GeoJson
				$feats = array();
				foreach ($res as $cell) {
					if (
						intval($cell['i']) % $skip != 0 || intval($cell['j']) % $skip != 0
					) continue;

					$center = [floatval($cell['lon']), floatval($cell['lat'])];

					// Compute the arrow head point
					$u = floatval($cell['u']);
					$v = floatval($cell['v']);
					$dir = self::getWindDir($u,$v);
					$mod = sqrt($u*$u + $v*$v);

					// Create an arrow geom
					$pnt = new Point($center);

					// Add some properties
					$prop = [
						"modulus" => $mod,
						"direction" => $dir
					];

					// Create the feature
					array_push($feats, new Feature($pnt, $prop));
				}

				// Create a feature collection
				$featcollect = new FeatureCollection($feats);
			}

			return isset($featcollect) ? $featcollect : null;
		}
	}

	/**
	 * Put the plotting weather data points from content to DB
	 */
	static function getPlotData ($variable,$datetime,$pressure) {
		if ($variable === "temperature" || $variable === "wind") {
			// No plot for temperature
			return null;
		}

		try {
			if ($pressure == 9999) {
				$variable = "surface";
			}

			if ($variable === "rain") {
				$table = "rain";
			} else if ($variable === "temperature") {
				// No plot for temperature
				return null;
			} else {
				$table = "plot_{$variable}";
			}

			if ($variable == "height") {
				$vars = "staid,height,t,delta_dew,wd,ws";
			} elseif ($variable == "surface") {
				$vars = "staid,wd,ws,p,dp3,prep,dew,visibility,t,dt,dp24";
			} elseif ($variable == "ecmwf_height") {
				$vars = "staid,value";
			} elseif ($variable == "ecmwf_wind") {
				//$vars = "staid,wd,ws";
				$vars = "staid,height,t,delta_dew,wd,ws";
			} elseif ($variable == "rain") {
				$vars = "staid,value";
			}
			// SQL
			$sql =  "
					SELECT ST_X(geom) as lon,ST_Y(geom) as lat,ST_Z(geom) as alti,{$vars}
					FROM {$table}
					WHERE datetime='{$datetime}'
					";
			if (isset($pressure) && !empty($pressure) && $pressure != 9999) {
				$sql .= " AND pressure={$pressure}";	
			}

			$db = \ORM::get_db();
			$q = $db->prepare($sql);
			if ( $q->execute() ) {
				$feats = array();
				while ( $row = $q->fetch(\PDO::FETCH_ASSOC) ) {
					$center = [floatval($row['lon']), floatval($row['lat'])];
					// Create an arrow geom
					$pnt = new Point($center);
					// Add some properties
					$prop = array();
					foreach ($row as $k => $v) {
						if ($k != 'lat' && $k != 'lon') {
							$prop[$k] = $v;
						}
					}

					// Create the feature
					array_push($feats, new Feature($pnt, $prop));
				}
				// Create a feature collection
				$featcollect = new FeatureCollection($feats);
			}
		} catch (\Exception $ex) {
			throw $ex;
		}

		return $featcollect;
	}
	
	/**
	 * Put the plotting weather data points from content to DB
	 */
	static function putPlotData ($content, $variable) {
		// Put data into DB
		$pattern = "/\s+/";
		$eolPattern = "/((\r?\n)+|(\r\n?)+)/";
		$il = 0;
		try {

			// Get the weather ground variable
			//var_dump($variable);
			if(isset($variable) == false) {
				throw new \Exception("The ground weather variable [{$variable}] was not found!");
			}//*/

			/////////////////////////////////////
			// Read data
			/////////////////////////////////////
			$data = array();
			
			$lines = preg_split($eolPattern, $content);

			// First line
			$values = explode(" ",trim(preg_replace("/[^0-9]+/", " ", $lines[$il])));
			$il++;

			
			// Get the date line
			$values = explode(";",preg_replace($pattern, ";", trim($lines[$il])));
			$timestamp = mktime(intval($values[3]),0,0, intval($values[1]), intval($values[2]), intval($values[0])+2000);
			$datetime = date("c", $timestamp);

			if($variable === "surface") {
				$nmeas = intval($values[4]);
			} else if ($variable === "height" || $variable === "ecmwf_wind") {
				$nmeas = intval($values[5]);
				$pressure = intval($values[4]);
			} else if ($variable === "ecmwf_height") {
				$nmeas = intval($values[10]);
				$pressure = intval($values[4]);
			}
			$il++;
			
			// Skip 11 lines in case of rain data
			if ($variable === "rain") {
				for ($i=0; $i<11; $i++) {
					$values = explode(";",preg_replace($pattern, ";", trim($lines[$il])));
					$il++;
				}
				// Get the number of mesures
				$values = explode(";",preg_replace($pattern, ";", trim($lines[$il])));
				$il++;
				$nmeas = intval($values[1]);
			}

			// Get the data array
			$data = array();
			$ldat = array();
			while ( $il < count($lines) ){
				if(!empty($lines[$il])) {
					if ($variable === "rain") {
						$values = explode(";",preg_replace($pattern, ";", trim($lines[$il])));
						$ldat = array_merge($ldat, array_map('floatval', $values));
						$value = array_splice($ldat, 1, 3);
						array_push($ldat, "ST_MakePoint({$value[0]}, {$value[1]}, {$value[2]})");
						array_push($ldat, "'$datetime'");

						// Put the line data
						$data[] = implode(",", $ldat);
					} else if ($variable === "ecmwf_height") {
						$values = explode(";",preg_replace($pattern, ";", trim($lines[$il])));
						$ldat = array_merge($ldat, array_map('floatval', $values));
						$value = array_splice($ldat, 1, 3);
						array_push($ldat, "ST_MakePoint({$value[0]}, {$value[1]}, {$value[2]})");
						array_push($ldat, "'$datetime'");
						if(isset($pressure)) {
							array_push($ldat, "$pressure");
						}

						// Put the line data
						$data[] = implode(",", $ldat);
					} else if ($variable === "height" || $variable === "ecmwf_wind") {
						$values = explode(";",preg_replace($pattern, ";", trim($lines[$il])));
						$ldat = array_merge($ldat, array_map('floatval', $values));
						$value = array_splice($ldat, 1, 4);
						array_push($ldat, "ST_MakePoint({$value[0]}, {$value[1]}, {$value[2]})");
						array_push($ldat, "'$datetime'");
						if(isset($pressure)) {
							array_push($ldat, "$pressure");
						}

						// Put the line data
						$data[] = implode(",", $ldat);
					} else if ($variable === "surface") {
						$values = explode(";",preg_replace($pattern, ";", trim($lines[$il])));
						$il++;
						$values = array_merge($values, explode(";",preg_replace($pattern, ";", trim($lines[$il]))));
						$ldat = array_merge($ldat, array_map('floatval', $values));
						$values = array();
						$values[] = $ldat[0];
						$values[] = $ldat[6]; // WD
						$values[] = $ldat[7]; // WS
						$values[] = $ldat[8]; // P
						$values[] = $ldat[9]; // deltaP(3h)
						$values[] = $ldat[12]; // prep
						$values[] = $ldat[16]; // dew
						$values[] = $ldat[17]; // visibility
						$values[] = $ldat[19]; // T
						$values[] = $ldat[24]; // deltaT
						$values[] = $ldat[25]; // deltaP(24h)
						array_push($values, "ST_MakePoint({$ldat[1]}, {$ldat[2]}, {$ldat[3]})");
						array_push($values, "'$datetime'");

						// Put the line data
						$data[] = implode(",", $values);
					} else {
						throw new \Exception("The weather variable [{$variable}] is not taken into account!", 500);
					}
					
					// Init the line array
					$ldat = array();
				}
				$il++;
			} 

			// Check the size of the line data
			if(count($data) != $nmeas) {
				throw new \Exception("The number of values in y [".count($data)."] is not equal ".$nmeas);
			}

			/////////////////////////////////////
			// Add data as raster into DB
			/////////////////////////////////////

			if ($variable === "rain") {
				$table = "rain";	
			} else {
				$table = "plot_{$variable}";
			}

			$cols = "";
			if ($variable === "surface") {
				$cols = "staid,wd,ws,p,dp3,prep,dew,visibility,t,dt,dp24,geom,datetime";
			} else if ($variable === "height" || $variable === "ecmwf_wind") {
				$cols = "staid,height,T,delta_dew,wd,ws,geom,datetime,pressure";
			} else if ($variable === "ecmwf_height") {
				$cols = "staid,value,geom,datetime,pressure";
			}  else if ($variable === "rain") {
				$cols = "staid,value,geom,datetime";
			}

			// String encode the data array
			$values = implode("),(", $data);

			// SQL
			$sql =  "INSERT INTO {$table} ({$cols}) VALUES ({$values})";
			//*/
			//\ORM::get_db()->beginTransaction();
			//echo $sql . '<br>';
			$ret = \ORM::get_db()->exec($sql);
			//*/
		} catch (\Exception $ex) {
			//$ret = \ORM::get_db()->rollBack();
			throw $ex;
		}

		return (!empty($ret)) ? $ret : false;
	}


	/**
	 * Put the ground weather data from content string
	 */
	static function putObservationData ($content, $variable) {
		// Put data into DB
		$pattern = "/\s+/";
		$eolPattern = "/((\r?\n)+|(\r\n?)+)/";
		$il = 0;
		try {

			/////////////////////////////////////
			// Read data
			/////////////////////////////////////
			$data = array();
			
			$lines = preg_split($eolPattern, $content);

			// First line
			$values = explode(" ",trim(preg_replace("/[^0-9]+/", " ", $lines[$il])));
			$il++;

			// Skip the empty line
			while (empty($lines[$il]) && $il<$nlines) {
				$il++;
			}
			
			// Get the date line
			$values = explode(";",preg_replace($pattern, ";", trim($lines[$il])));
			$il++;
			$timestamp = mktime(intval($values[3]),0,0, intval($values[1]), intval($values[2]), intval($values[0])+2000);
			$datetime = date("c", $timestamp);
			//var_dump($datetime);
			// Get the pressure
			if (isset($values[5])) {
				$pressure = intval($values[5]);
			}

			// Skip the empty line
			while (empty($lines[$il]) && $il<$nlines) {
				$il++;
			}

			// Get the domain
			$values = explode(";",preg_replace($pattern, ";", trim($lines[$il])));
			$il++;
			$dom = array(
				"dx" => floatval($values[0]), "dy" => floatval($values[1]),
				"lonSW" => floatval($values[2]), "lonNE" => floatval($values[3]),
				"latSW" => floatval($values[4]), "latNE" => floatval($values[5])
				);

			// Get the grid information line
			if ($variable === "wind") {
				$grid = array("nx" => intval($values[6]), "ny" => intval($values[7]));
			} else {
				$values = explode(";",preg_replace($pattern, ";", trim($lines[$il])));
				$il++;
				$grid = array("nx" => intval($values[0]), "ny" => intval($values[1]));
			}
			//var_dump($grid);

			// Get the data array
			$data = array();
			$ldat = array();
			if($grid["nx"] % 10 != 0) {
				$mod = intval($grid["nx"]/10) + 1;
			} else {
				$mod = intval($grid["nx"]/10);
			}
			$idl = $il;
			while ( $il < count($lines) ){
				if(!empty($lines[$il])) {
					$values = explode(";",preg_replace($pattern, ";", trim($lines[$il])));
					$ldat = array_merge($ldat, array_map('floatval', $values));

					if(($il-$idl) % $mod == $mod - 1) {
						// Check the size of the line data
						if(count($ldat) != $grid["nx"]) {
							throw new \Exception("The number of values in x is not equal ".$grid["nx"]);
						}
						// Put the line data
						$data[] = $ldat;
						// Init the line array
						$ldat = array();
					}
				}
				$il++;
			} 

			// Check the size of the line data
			$readlines = ($variable === "wind") ? 2*$grid["ny"] : $grid["ny"];
			if ( count($data) != $readlines ) {
				throw new \Exception("The number of values in y [".count($data)."] is not equal ".$readlines);
			}

			/////////////////////////////////////
			// Add data as raster into DB
			/////////////////////////////////////

			$table = "observation";

			// Preparation
			$nx = $grid["nx"];$ny=$grid["ny"];
			$upperleftx = $dom["lonSW"];
			if ($dom["dy"] > 0) {
				$upperlefty = $dom["latSW"];
			} else {
				$upperlefty = $dom["latNE"];
			}
			$dx = $dom["dx"];$dy = $dom["dy"];
			$bandId = 1;

			// String encode the data array
			if ($variable === "wind") {
				// Set U,V arrays
				$data = array_chunk($data, $grid["ny"]);
				// String encode the data array
				$uvalues = "ARRAY".json_encode($data[0])."::double precision[][]";
				$vvalues = "ARRAY".json_encode($data[1])."::double precision[][]";
			} else {
				$values = "ARRAY".json_encode($data)."::double precision[][]";
			}


			$cols = "rast, datetime, pressure, variable";
			// SQL
			if ($variable === "wind") {
				// SQL
				$sql =  "
					INSERT INTO {$table} ({$cols})
					VALUES (
					ST_SetValues(
						ST_SetValues(
	 						ST_AddBand(
	 							ST_MakeEmptyRaster({$nx}, {$ny}, {$upperleftx}, {$upperlefty}, {$dx}, {$dy}, 0, 0, 4326),
 								ARRAY[
 									ROW(1, '32BF', -999., 0),
 									ROW(2, '32BF', -999., 0)
								]::addbandarg[]
	 						),
	 						1, 1, 1,
	 						{$uvalues}
 						),
						2, 1, 1,
						{$vvalues}
					),
					'{$datetime}',{$pressure},'{$variable}'
					)
				";
			} else {
				$sql =  "
					INSERT INTO {$table} ({$cols})
					VALUES (
						ST_SetValues(
								ST_AddBand(
									ST_MakeEmptyRaster({$nx}, {$ny}, {$upperleftx}, {$upperlefty}, {$dx}, {$dy}, 0, 0, 4326),
									{$bandId}, '32BF', -999., 0
								),
								{$bandId}, 1, 1,
								{$values}
							), 
						'{$datetime}',{$pressure},'{$variable}'
					)
				";
			}
			//echo $sql . '<br>';
			$ret = \ORM::get_db()->exec($sql);
			//
		} catch (\Exception $ex) {
			throw $ex;
		}

		return (!empty($ret)) ? $ret : false;
	}

	/**
	 * Read the missile trajectory data from file
	 */
	static function readMissileData ($content) {
		// Put data into DB
		$pattern = "/\s+/";
		$eolPattern = "/((\r?\n)|(\r\n?))/";
		$il = 0;
		try {

			/////////////////////////////////////
			// Read data
			/////////////////////////////////////
			$data = array();
			
			$lines = preg_split($eolPattern, $content);
			$nlines = count($lines);

			// First line
			$values = explode(" ",trim(preg_replace("/[^0-9]+/", " ", $lines[$il])));
			$il++;

			// Skip the empty line
			while (empty($lines[$il]) && $il<$nlines) {
				$il++;
			}
			
			// Get the date line
			$values = explode(";",preg_replace($pattern, ";", trim($lines[$il])));
			$il++;
			$timestamp = mktime(intval($values[3]),0,0, intval($values[1]), intval($values[2]), intval($values[0])+2000);
			$datetime = date("c", $timestamp);
			//var_dump($datetime);

			// Get the data array
			$data = array();
			while($il<count($lines)) {

				$values = explode(";",preg_replace($pattern, ";", trim($lines[$il])));
				if (count($values) < 6) {
					continue;
				}
				
				$ldat = array_map('floatval', $values);

				// Put the line data
				$data[] = array_merge(array_slice($ldat, 0, 3), array_slice($ldat, 5, 1));

				$il++;
			}

			// Check the size of the line data
			if(empty($data)) {
				throw new \Exception("No data found in file.");
			}

			/////////////////////////////////////
			// Add data as raster into DB
			/////////////////////////////////////

			// String encode the data array
			$values = array();
			for ($i=0;$i<count($data);$i++) {
				$values[] = "(
					{$data[$i][0]},{$data[$i][1]},{$data[$i][2]},{$data[$i][3]}
					)";
			}
			$values = implode(",", $values);

			$table = "missile_path";
			// SQL
			$sql =  "
				INSERT INTO {$table} (status, lon, lat, height) VALUES 
				". $values;
			// Exec
			$ret = \ORM::get_db()->exec($sql);

			return $ret;
		} catch (\Exception $ex) {
			throw $ex;
		}
	}

	/**
	 * Put the EC weather data from content string
	 */
	static function putEcmwfData ($content, $variable) {
		// Put data into DB
		$pattern = "/\s+/";
		$eolPattern = "/((\r?\n)+|(\r\n?)+)/";
		$il = 0;
		try {

			/////////////////////////////////////
			// Read data
			/////////////////////////////////////
			$data = array();
			
			$lines = preg_split($eolPattern, $content);
			$nlines = count($lines);

			// First line
			$values = explode(" ",trim(preg_replace("/[^0-9]+/", " ", $lines[$il])));
			$il++;

			// Skip the empty line
			while (empty($lines[$il]) && $il<$nlines) {
				$il++;
			}
			
			// Get the date line
			$values = explode(";",preg_replace($pattern, ";", trim($lines[$il])));
			$il++;
			$timestamp = mktime(intval($values[3]),0,0, intval($values[1]), intval($values[2]), intval($values[0])+2000);
			$datetime = date("c", $timestamp);
			//var_dump($datetime);
			// Get the pressure
			if (isset($values[5])) {
				$pressure = intval($values[5]);
			}

			// Get the domain
			if ($variable === "thin") {
				$values = explode(";",preg_replace($pattern, ";", trim($lines[$il])));
				$il++;
				$dom = array(
					"dx" => floatval($values[0]), "dy" => floatval($values[1]),
					"lonSW" => floatval($values[2]), "lonNE" => floatval($values[3]),
					"latSW" => floatval($values[4]), "latNE" => floatval($values[5])
					);
			} else {
				$dom = array(
					"dx" => floatval($values[6]), "dy" => floatval($values[7]),
					"lonSW" => floatval($values[8]), "lonNE" => floatval($values[9]),
					"latSW" => floatval($values[10]), "latNE" => floatval($values[11])
					);
			}
			//var_dump($dom);

			// Get the grid information line
			if ($variable === "thin") {
				$grid = array("nx" => intval($values[6]), "ny" => intval($values[7]));
			} else if ($variable === "wind") {
				$grid = array("nx" => intval($values[12]), "ny" => intval($values[13]));
			} else {
				$grid = array("nx" => intval($values[12]), "ny" => intval($values[13]));
			}
			//var_dump($grid);

			// Get the data array
			$data = array();
			$ldat = array();
			if($grid["nx"] % 10 != 0) {
				$mod = intval($grid["nx"]/10) + 1;
			} else {
				$mod = intval($grid["nx"]/10);
			}

			$idl = $il;
			while ( $il < $nlines ){
				if(!empty($lines[$il])) {
					$values = explode(";",preg_replace($pattern, ";", trim($lines[$il])));
					$ldat = array_merge($ldat, array_map('floatval', $values));

					if(($il-$idl) % $mod == $mod - 1) {
						// Check the size of the line data
						if(count($ldat) != $grid["nx"]) {
							throw new \Exception("The number of values in x is not equal ".$grid["nx"]);
						}
						// Put the line data
						$data[] = $ldat;
						// Init the line array
						$ldat = array();
					}
				}
				$il++;
			}


			// Check the size of the line data
			$readlines = ($variable === "wind" || $variable === "thin") ? 2*$grid["ny"] : $grid["ny"];
			if ( count($data) != $readlines ) {
				throw new \Exception("The number of values in y [".count($data)."] is not equal ".$readlines);
			}


			/////////////////////////////////////
			// Add data as raster into DB
			/////////////////////////////////////

			$table = "ecmwf";

			// Preparation
			$nx = $grid["nx"];$ny=$grid["ny"];
			$upperleftx = $dom["lonSW"];$upperlefty = $dom["latSW"];
			$dx = $dom["dx"];$dy = $dom["dy"];
			$bandId = 1;

			// String encode the data array
			if ($variable === "wind" || $variable === "thin") {
				// Set U,V arrays
				$data = array_chunk($data, $grid["ny"]);
				// String encode the data array
				$uvalues = "ARRAY".json_encode($data[0])."::double precision[][]";
				$vvalues = "ARRAY".json_encode($data[1])."::double precision[][]";
			} else {
				$values = "ARRAY".json_encode($data)."::double precision[][]";
			}


			$cols = "rast, datetime, pressure, variable";
			// SQL
			if ($variable === "wind" || $variable === "thin") {
				// SQL
				$sql =  "
					INSERT INTO {$table} ({$cols})
					VALUES (
					ST_SetValues(
						ST_SetValues(
	 						ST_AddBand(
	 							ST_MakeEmptyRaster({$nx}, {$ny}, {$upperleftx}, {$upperlefty}, {$dx}, {$dy}, 0, 0, 4326),
 								ARRAY[
 									ROW(1, '32BF', -999., 0),
 									ROW(2, '32BF', -999., 0)
								]::addbandarg[]
	 						),
	 						1, 1, 1,
	 						{$uvalues}
 						),
						2, 1, 1,
						{$vvalues}
					),
					'{$datetime}',{$pressure},'{$variable}'
					)
				";
			} else {
				$sql =  "
					INSERT INTO {$table} ({$cols})
					VALUES (
						ST_SetValues(
								ST_AddBand(
									ST_MakeEmptyRaster({$nx}, {$ny}, {$upperleftx}, {$upperlefty}, {$dx}, {$dy}, 0, 0, 4326),
									{$bandId}, '32BF', -999., 0
								),
								{$bandId}, 1, 1,
								{$values}
							), 
						'{$datetime}',{$pressure},'{$variable}'
					)
				";
			}

			$ret = \ORM::get_db()->exec($sql);
			//
		} catch (\Exception $ex) {
			throw $ex;
		}

		return (!empty($ret)) ? $ret : false;
	}

	/**
	 * Put the rain data from content string
	 */
	static function putRainData ($content) {
		// Put data into DB
		$pattern = "/\s+/";
		$eolPattern = "/((\r?\n)|(\r\n?))/";
		try {

			/////////////////////////////////////
			// Read data
			/////////////////////////////////////
			$data = array();
			
			$lines = preg_split($eolPattern, $content);
			
			// Get the date line
			$values = explode(";",preg_replace($pattern, ";", trim($lines[1])));
			$timestamp = mktime(intval($values[3]),0,0, intval($values[1]), intval($values[2]), intval($values[0])+2000);
			$datetime = date("c", $timestamp);
			//var_dump($datetime);

			// Get the number of rain stations
			$values = explode(";",preg_replace($pattern, ";", trim($lines[13])));
			$nsta = intval($values[1]);

			// Get the data array
			$il = 14;
			while ( $il < count($lines) ){
				if(!empty($lines[$il])) {
					$values = explode(";",preg_replace($pattern, ";", trim($lines[$il])));
					// Put the line data
					$data[] = array_map('floatval', $values);
				}
				$il++;
			} 

			// Check the size of the line data
			if(count($data) != $nsta) {
				throw new \Exception("The number of station data lines [".count($data)."] is not equal to ".$nsta);
			}

			/////////////////////////////////////
			// Add data as raster into DB
			/////////////////////////////////////

			// String encode the data array
			$values = array();
			for ($i=0;$i<count($data);$i++) {
				$values[] = "(
					{$data[$i][0]},{$data[$i][1]},{$data[$i][2]},{$data[$i][3]},{$data[$i][4]},
					'$datetime'
					)";
			}
			$values = implode(",", $values);

			$table = $this->rainTable;
			// SQL
			$sql =  "
				INSERT INTO {$table} (staid, lon, lat, elevation, value, datetime) VALUES 
				". $values;
			// Exec
			$ret = \ORM::get_db()->exec($sql);

			return $ret;
		} catch (\Exception $ex) {
			throw $ex;
		}
	}


	///////////////////////////////////////////////////////
	// Helpers
	///////////////////////////////////////////////////////
	static function getMetaData($table, $variable, $timeseq, $pressure) {

		// SQL
		$sql =  "
			SELECT (ST_MetaData(rast)).*
			FROM {$table}
			WHERE variable='{$variable}' AND datetime='{$timeseq}' AND pressure = {$pressure}
			";

		// Query
		try {
			$q = \ORM::get_db()->prepare($sql);
			if ( $q->execute() ) {
				$data = $q->fetch(\PDO::FETCH_ASSOC);
			}
		} catch (\PDOException $ex) {
			throw $ex;
		}

		return (isset($data)) ? $data : null;
	}

	static function getStats ($table, $variable, $timeseq, $pressure) {
		// SQL
		$sql =  "
		SELECT (ST_SummaryStats(rast)).*
		FROM {$table}
		WHERE variable='{$variable}' AND datetime='{$timeseq}' AND pressure = {$pressure}
		";

		// Query
		try {
			$q = \ORM::get_db()->prepare($sql);
			if ($q->execute() ) {
				$data = $q->fetch(\PDO::FETCH_ASSOC);
			}
		} catch (\PDOException $ex) {
			throw $ex;
		}

		return $data ? array_map('floatval',$data) : null;
	}

}