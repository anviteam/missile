<?php

require 'vendor/autoload.php';

define('__ROOT__', dirname(dirname(__FILE__)));
define('__APP__', dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'api');
define('__MODEL__', __APP__.DIRECTORY_SEPARATOR.'model');
define('__CONTROLLER__', __APP__.DIRECTORY_SEPARATOR.'controller'); 
define('__HELPER__', __APP__.DIRECTORY_SEPARATOR.'helper');
define('__DATA__', __ROOT__.DIRECTORY_SEPARATOR.'data');
define('__RUNTIME__', __APP__.DIRECTORY_SEPARATOR.'runtime');


use AnVi\Model\GroundWeather;
use Anvi\MissilePath;
use AnVi\Model\Radar;
use Anvi\Helper\IO;


// DB ORM Configuration
// =======================================================================
// PARIS package
require_once(__APP__.DIRECTORY_SEPARATOR.'vendor\j4mie\idiorm\idiorm.php');
require_once(__APP__.DIRECTORY_SEPARATOR.'vendor\j4mie\paris\paris.php');


ORM::configure('pgsql:host=localhost;dbname=weather');
ORM::configure('username', 'postgres');
ORM::configure('password', 'viet');
ORM::configure('logging', true);
// =======================================================================


// =======================================================================
// Create and configure Slim app
// =======================================================================
$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];
$c = new \Slim\Container($configuration);
$c['errorHandler'] = function ($c) {
    return function ($request, $response, $exception) use ($c) {
        return $c['response']->withStatus(500)
                             ->withHeader('Content-Type', 'application/json')
                             ->write(json_encode(array(
                             	"error"=>$exception->getCode(),
                             	"message"=>$exception->getMessage()
                             	)));
    };
};
$app = new \Slim\App($c);
// =======================================================================


// Define app routes
$app->group('/data', function () {

	/////////////////////////////////////////////////
	// GET Data
	/////////////////////////////////////////////////

 	$this->get('/radar/{datetime}/{format}', 'Anvi\Controller\RadarController:getImage');
 	$this->get('/radar/{datetime}', 'Anvi\Controller\RadarController:getMeta');

 	$this->get('/satellite/{datetime}/{format}', 'Anvi\Controller\SatelliteController:getImage');
 	$this->get('/satellite/{datetime}', 'Anvi\Controller\SatelliteController:getMeta');
 	
 	$this->get('/missile/{datetime}', 'Anvi\Controller\MissileController:getPath');
 	$this->get('/missiles', 'Anvi\Controller\MissileController:getAllPaths');
 	$this->get('/ec/{variable}[/{datetime}[/{pressure}]]', 'Anvi\Controller\WindController:getWindVelocity');

 	$this->get('/plot/{variable}[/{datetime}[/{pressure}]]', 'Anvi\Controller\PlotController:get');
 	$this->get('/observation/{variable}[/{datetime}[/{pressure}]]', 'Anvi\Controller\ObservationController:get');

 	// Get the result
 	$this->get('/result', function ($request, $response, $args) {
 		try {
            $resp = $response->withHeader('Content-type', 'application/json');
            $ret = array('result'=>"No content", 'error' => 0);

        	$ret["result"] = file_get_contents("http://localhost:8000/missile/data/result.res");

            $resp->getBody()->write(json_encode($ret));
            return $resp;
        } catch (\Exception $ex) {
            throw $ex;
        }
 	});

 	// Get all time sequences of the database
	$this->get('/times', function ($request, $response, $args) {
		try {
            $resp = $response->withHeader('Content-type', 'application/json');
            $ret = array('message'=>"No content", 'error' => 0);

        	$ret = IO::getTimeSequences ();

            $resp->getBody()->write(json_encode($ret));

            return $resp;
        } catch (\Exception $ex) {
            throw $ex;
        }
	});

    $this->get('/run[/{params}]', function ($request, $response, $args) {
        try {
            $resp = $response->withHeader('Content-type', 'application/json');
            $ret = array('message'=>"No content", 'error' => 0);

            // Run ReadRadar runtime
            $params = "";
            if (isset($args['params'])) {
                $params = $args['params'];
            }
            $param_file = __RUNTIME__.DIRECTORY_SEPARATOR.$params;
            $prog = __RUNTIME__.DIRECTORY_SEPARATOR.'run.bat';

            if (empty($params)) {
                $cmd = "{$prog}";
            } else {
                $cmd = "{$prog} {$param_file}";
            }
            exec($cmd, $output, $retval);

            // Save the output into db
            if(!empty($output)) {
                $ret['output'] = $output;
                $ret['error'] = $retval;
            }

            $resp->getBody()->write(json_encode($ret));

            return $resp;
        } catch (\Exception $ex) {
            throw $ex;
        }
    });
 	/////////////////////////////////////////////////
	// SET Data
	/////////////////////////////////////////////////
	$this->post('/radar', 'Anvi\Controller\RadarController:setData');

	$this->post('/satellite', 'Anvi\Controller\SatelliteController:setData');
	$this->post('/ec/{variable}', 'Anvi\Controller\WindController:setData');

	$this->post('/observation/{variable}', 'Anvi\Controller\ObservationController:set');
	$this->post('/plot/{variable}', 'Anvi\Controller\PlotController:set');

	$this->post('/missile', 'Anvi\Controller\MissileController:set');

});

// Run app
$app->run();

//var_dump(memory_get_peak_usage(true));
//*/
