<?php

namespace Anvi\Model;

class MissilePath extends \Model
{
	/**
     * The table associated with the model.
     *
     * @var string
     */
    public static $_table = 'missile_path';

    public static $_id_column = 'id';

    //public static $_table_use_short_name = true;

	public static function toWKT($datetime, $type = null) {

		// WKT
		$linStart = "LINESTRING(";
		$linEnd = ")";
		$multilinStart = "MULTILINESTRING(";
		$multilinEnd = ")";
		$multipntStart = "MULTIPOINT(";
		$multipntEnd = ")";
		$pntStart = "POINT(";
		$pntEnd = ")";

		// Fetch all
		$data = \Model::factory('\Anvi\Model\MissilePath')
			->where('datetime',$datetime)
   			->find_many();

		try {
			$coords = array();
			foreach ($data as $pos) {
				//
				$lon = $pos->lon;
				$lat = $pos->lat;
				$status = $pos->status;

				if($type === "MULTILINESTRING") {
					$coords[] = null;
				} else if ($type === "POINT") {
					$coords[] = array(
						'status'=>$status,
						'wkt'=>$pntStart."{$lon} {$lat}".$pntEnd);
				} else { // Default is LINESTRING
					$coords[] = "{$lon} {$lat}";
				}

			}

			if ($type === "POINT") {
				return $coords;
			} else {
				// Make the WKT string
				if ($type === "LINESTRING") {
					$wkt = $linStart.implode(",",$coords).$linEnd;
				} else if ($type === "MULTILINESTRING") {
					$wkt = $multilinStart.implode(",",$coords).$multilinEnd;
				} else if ($type === "MULTIPOINT") {
					$wkt = $multipntStart.implode(",",$coords).$multipntEnd;
				} else {
					$wkt = $multipntStart.implode(",",$coords).$multipntEnd;
				}

				return array('wkt' => $wkt);
			}
		} catch (\Exception $ex) {
			throw $ex;
		}
	}
}