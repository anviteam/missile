<?php

namespace AnVi\Model;

use GeoJson\GeoJson;
use GeoJson\Geometry\Point;
use GeoJson\Feature\Feature;
use GeoJson\Feature\FeatureCollection;

class ECWind extends \Model {

	/**
     * The table associated with the model.
     *
     * @var string
     */
    public static $_table = 'observation';

    /**
     * * @var string
     */
    public static $_id_column = 'rid';

    public static function getWindDir ($u, $v) {
    	$r2d = (180./M_PI);

    	if($u < 0) {
    		if ($v != 0) {
    			$a = 90. - atan($v/$u)*$r2d;
    		} else {
    			$a = 90.;
    		}
    	} else if ($u > 0) {
    		if ($v != 0) {
    			$a = 270. - atan($v/$u)*$r2d;
    		} else {
    			$a = 270.;
    		}
    	} else {
    		if ($v > 0) {
    			$a = 180.;
    		} else if ($v < 0) {
    			$a = 360.;
    		} else {
    			$a = 0;
    		}
    	}

    	return $a;
    }

    /**
     * Get the u,v fields as GeoJson format
     */
    public static function getData($variable, $time, $pressure, $skip = 5) {
    	$db = \ORM::get_db();
    	$table = self::$_table;
    	$sql ="
    		WITH foo_u as (
				SELECT (ST_PixelAsCentroids(rast, 1)).* FROM {$table}
				WHERE variable='{$variable}' AND datetime='{$time}' AND pressure={$pressure}
			),
			foo_v as (
				SELECT (ST_PixelAsCentroids(rast, 2)).* FROM {$table}
				WHERE variable='{$variable}' AND datetime='{$time}' AND pressure={$pressure}
			)
			SELECT foo_u.x as i, foo_u.y as j, foo_u.val as u, foo_v.val as v, ST_X(foo_u.geom) as lon, ST_Y(foo_u.geom) as lat
			FROM foo_u JOIN foo_v ON foo_u.x = foo_v.x AND foo_u.y = foo_v.y
    	";

    	$res = array();
    	try {
			$q = $db->prepare($sql);
			if ( $q->execute() ) {
				while ( $row = $q->fetch(\PDO::FETCH_ASSOC) ) {
					array_push($res,$row);
				}
			}
		} catch (\PDOException $ex) {
			throw $ex;
		}
//var_dump(memory_get_peak_usage(true));
		// Output
		if(!empty($res)) {
			// Convert to GeoJson
			$feats = array();
			foreach ($res as $cell) {
				if (
					intval($cell['i']) % $skip != 0 || intval($cell['j']) % $skip != 0
				) continue;

				$center = [floatval($cell['lon']), floatval($cell['lat'])];

				// Compute the arrow head point
				$u = floatval($cell['u']);
				$v = floatval($cell['v']);
				$dir = self::getWindDir($u,$v);
				$mod = sqrt($u*$u + $v*$v);

				// Create an arrow geom
				$pnt = new Point($center);

				// Add some properties
				$prop = [
					"modulus" => $mod,
					"direction" => $dir
				];

				// Create the feature
				array_push($feats, new Feature($pnt, $prop));
			}

			// Create a feature collection
			$featcollect = new FeatureCollection($feats);
		}

		return isset($featcollect) ? $featcollect : null;
    }

	public static function saveData($content) {
        // Put data into DB
		$pattern = "/\s+/";
		$eolPattern = "/((\r?\n)|(\r\n?))/";
		try {

			/////////////////////////////////////
			// Read data
			/////////////////////////////////////
			$data = array();
			
			$lines = preg_split($eolPattern, $content);

			$il = 0;
			$nlines = count($lines);

			// Get the data label
			$datalabel = $lines[$il];
			$il++;

			// Skip the empty line
			while (empty($lines[$il]) && $il<$nlines) {
				$il++;
			}
			
			// Get the date line
			$values = explode(";",preg_replace($pattern, ";", trim($lines[$il])));
			$timestamp = mktime(intval($values[3]),0,0, intval($values[1]), intval($values[2]), intval($values[0]));
			$datetime = date("c", $timestamp);
			//var_dump($datetime);
			$il++;

			// Skip the empty line
			while (empty($lines[$il]) && $il<$nlines) {
				$il++;
			}

			// Get the domain
			$values = explode(";",preg_replace($pattern, ";", trim($lines[$il])));
			$dom = array(
				"dx" => floatval($values[0]), "dy" => floatval($values[1]),
				"lonSW" => floatval($values[2]), "lonNE" => floatval($values[3]),
				"latSW" => floatval($values[4]), "latNE" => floatval($values[5])
				);
			//var_dump($dom);

			// Get the grid information line
			$grid = array("nx" => intval($values[6]), "ny" => intval($values[7]));
			//var_dump($grid);
			$il++;

			// Skip the empty line
			while (empty($lines[$il]) && $il<$nlines) {
				$il++;
			}

			// Get the data array
			$data = array();
			$ldat = array();
			while( $il < $nlines ) {

				if(!empty($lines[$il])) {

					$values = explode(";",preg_replace($pattern, ";", trim($lines[$il])));
					$ldat = array_merge($ldat, array_map('floatval', $values));

					// Check the size of the line data
					if(count($ldat) != $grid["nx"]) {
						throw new \Exception("The number of values in x is not equal ".$grid["nx"]);
					}
					// Put the line data
					$data[] = $ldat;
					// Init the line array
					$ldat = array();
				}
				$il++;
			}

			// Check the size of the line data
			if(count($data) != 2*$grid["ny"]) {
				throw new \Exception("The number of values in y (".count($data).") is not equal ".$grid["ny"]);
			}

			// Set U,V arrays
			$data = array_chunk($data, $grid["ny"]);

			/////////////////////////////////////
			// Add data as raster into DB
			/////////////////////////////////////

			$table = self::$_table;

			// Preparation
			$nx = $grid["nx"];$ny=$grid["ny"];
			$upperleftx = $dom["lonSW"];$upperlefty = $dom["latSW"];
			$dx = $dom["dx"];$dy = $dom["dy"];
			$bandId = 1;

			// String encode the data array
			$uvalues = "ARRAY".json_encode($data[0])."::double precision[][]";
			$vvalues = "ARRAY".json_encode($data[1])."::double precision[][]";

			// SQL
			$sql =  "
					INSERT INTO {$table} (rast, time_sequence)
					VALUES (
					ST_SetValues(
						ST_SetValues(
	 						ST_AddBand(
	 							ST_MakeEmptyRaster({$nx}, {$ny}, {$upperleftx}, {$upperlefty}, {$dx}, {$dy}, 0, 0, 4326),
 								ARRAY[
 									ROW(1, '32BF', -999., 0),
 									ROW(2, '32BF', -999., 0)
								]::addbandarg[]
	 						),
	 						1, 1, 1,
	 						{$uvalues}
 						),
						2, 1, 1,
						{$vvalues}
					),
					'{$datetime}'
					)
					";
			//echo $sql . '<br>';
			$ret = \ORM::get_db()->exec($sql);
			//
		} catch (\PDOException $ex) {
			throw $ex;
		} catch (\Exception $ex) {
			throw $ex;
		}

		return (!empty($ret)) ? $ret : false;
    }

	public static function toByteImage () {
		/*
		$radarObj = \Model::factory('\Anvi\Model\ECWind')
			->find_one(1);
		return $radarObj->data;*/
	}
}