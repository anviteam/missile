<?php

namespace AnVi\Model;

use Anvi\Helper\GlobalMercator;

class Radar extends \Model {

	/**
     * The table associated with the model.
     *
     * @var string
     */
    public static $_table = 'radar';

    /**
     * * @var string
     */
    public static $_id_column = 'rid';

	// Properties
	private $contours = null;

	public static function toByteImage ($datetime) {
		$radarObj = \Model::factory('\Anvi\Model\Radar')
            ->where('datetime', $datetime)
			->find_one();
        
        if (empty($radarObj)) {
            throw new \Exception("The radar data at $datetime was not found");
        }

		return $radarObj->data;
	}

    public static function getMeta ($datetime) {
        $radarObj = \Model::factory('\Anvi\Model\Radar')
            ->where('datetime', $datetime)
            ->find_one();

        if (empty($radarObj)) {
            throw new \Exception("The radar data at $datetime was not found");
        }

        // Image size
        $imgsize = array_map('intval',preg_split("/x/i", $radarObj->imgsize));

        // Compute the image extent
        $mercator = new GlobalMercator();
        $center = $mercator->LatLonToMeters($radarObj->lat, $radarObj->lon);
        $xmin = $center[0] - $radarObj->xres*$imgsize[0]*0.5;
        $ymin = $center[1] - $radarObj->yres*$imgsize[1]*0.5;
        $xmax = $center[0] + $radarObj->xres*$imgsize[0]*0.5;
        $ymax = $center[1] + $radarObj->yres*$imgsize[1]*0.5;
        $extent = array($xmin,$ymin,$xmax,$ymax);

        return array(
            "lat"=>$radarObj->lat,
            "lon"=>$radarObj->lon,
            "extent"=>$extent,
            "projection"=>"EPSG:3857"
            );
    }
}