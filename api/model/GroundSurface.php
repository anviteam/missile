<?php

namespace AnVi\Weather;

class GroundSurface extends \Model {

	/**
     * The table associated with the model.
     *
     * @var string
     */
    public static $_table = 'surface';

    /**
     * * @var string
     */
    public static $_id_column = 'rid';

	// Properties
	private $contours = null;

	public static function toByteImage () {
		$radarObj = \Model::factory('\Anvi\Weather\GroundSurface')
			->find_one(1);
		return $radarObj->rast;
	}
}