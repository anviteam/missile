<?php

namespace AnVi\Model;

use Anvi\Helper\GlobalMercator;

class Satellite extends \Model {

	/**
     * The table associated with the model.
     *
     * @var string
     */
    public static $_table = 'satellite';

    /**
     * * @var string
     */
    public static $_id_column = 'rid';

	// Properties
	private $contours = null;

	public static function toByteImage ($datetime) {
		$satObj = \Model::factory('\Anvi\Model\Satellite')
            ->where('datetime', $datetime)
			->find_one();
        
        if (empty($satObj)) {
            throw new \Exception("The satellite data at $datetime was not found");
        }

		return $satObj->data;
	}

    public static function getMeta ($datetime) {
        $satObj = \Model::factory('\Anvi\Model\Satellite')
            ->where('datetime', $datetime)
            ->find_one();

        if (empty($satObj)) {
            throw new \Exception("The satellite data at $datetime was not found");
        }

        // Image size
        $imgsize = array_map('intval',preg_split("/x/i", $satObj->imgsize));

        // Compute the image extent
        $mercator = new GlobalMercator();
        $center = $mercator->LatLonToMeters($satObj->lat, $satObj->lon);
        $xmin = $center[0] - $satObj->xres*$imgsize[0]*0.5;
        $ymin = $center[1] - $satObj->yres*$imgsize[1]*0.5;
        $xmax = $center[0] + $satObj->xres*$imgsize[0]*0.5;
        $ymax = $center[1] + $satObj->yres*$imgsize[1]*0.5;
        $extent = array($xmin,$ymin,$xmax,$ymax);

        return array(
            "lat"=>$satObj->lat,
            "lon"=>$satObj->lon,
            "extent"=>$extent,
            "projection"=>"EPSG:3857"
            );
    }
}