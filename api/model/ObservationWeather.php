<?php

namespace Anvi\Model;

use Anvi\Helper\ClosedConrec;
use Anvi\Helper\IO;

use GeoJson\GeoJson;
use GeoJson\Geometry\Point;
use GeoJson\Geometry\Polygon;
use GeoJson\Feature\Feature;
use GeoJson\Feature\FeatureCollection;

class ObservationWeather {

	// Properties
	private $x = array();
	private $y = array();
	private $d = null;
	private $nc = 10;
	private $srid = null;
	private $stats = null;

	private $upperleftx = null;
	private $upperlefty = null;

	private $contours = null;
	private $byta = null;

	// Local variables
	private $table = "observation";

	// WKT
	private $polStart = "POLYGON((";
	private $polEnd = "))";
	private $linStart = "LINESTRING(";
	private $linEnd = ")";
	private $multilinStart = "MULTILINESTRING(";
	private $multilinEnd = ")";

	/**
	 * Construction of a object from DB
	 *
	 * Rast metadata output :
	 * upperleftx | upperlefty | width | height | scalex | scaley | skewx | skewy | srid | numbands
	 */
	public function __construct ($variable, $time, $pressure) {
		try {
			// Get the meta data of the rast
			$meta = IO::getMetaData($this->table, $variable, $time, $pressure);
			//var_dump($meta);
			// Get stats
			$this->stats = IO::getStats($this->table, $variable, $time, $pressure);
			//var_dump($this->stats);
			/////////////////////////////////////
			// Set the properties
			/////////////////////////////////////
			// x[]
			for ($i = 0; $i<$meta['width']; $i++) {
				$this->x[] = $meta['upperleftx'] + $i*$meta['scalex'];
			}
			//var_dump($this->x);
			// y[]
			$lowerlefty = $meta['upperlefty'] - ($meta['height']-1)*$meta['scaley'];
			for ($j = 0; $j<$meta['height']; $j++) {
				$this->y[] = $lowerlefty + $j*$meta['scaley'];
			}
			//var_dump($this->y);
			// d[][]
			$this->d = IO::getObservationData($this->table, $variable, $time, $pressure);
			//var_dump($this->d);return;

			// SRID
			$this->srid = $meta['srid'];

			// Create the isocontours
			//$this->contours = CONREC_contour($this->d,$this->y,$this->x,$this->nc);
			//$cc = new \Anvi\Helper\ClosedConrec();
			$cc = new ClosedConrec();
			$cc->CONREC_contour($this->d,$this->y,$this->x,$this->nc);
			$this->contours = $cc->contourList();

			// Get the byte array
			//$this->byta = $db->getDataToImage($variable, $rid);//var_dump($this->byta);
		} catch (\Exception $ex) {
			throw $ex;
		}
	}

	public function render () {
		return $this->byta;
	}

	public function getStats () {
		return $this->stats;
	}

	function toGeoJson () {
		if(!empty($this->contours)) {
			$feats = array();
			foreach ($this->contours as $contour) {

				// Add some properties
				$prop = [
					"value" => $contour['level'],
					"rotation" => $contour['rotation']
				];

				// Create the feature
				array_push($feats, new Feature($contour['geometry'], $prop));
			}

			// Create a feature collection
			$featcollect = new FeatureCollection($feats);
		}

		return isset($featcollect) ? $featcollect : false;
	}

	function toKML () {
		// Call conrec
		if(empty($this->contours)) 
			$this->contours = CONREC_contour($this->d,$this->y,$this->x,$this->nc);

		return $this->contours;
	}

	function toIsoContours () {
		// Call conrec
		if(empty($this->contours)) 
			$this->contours = CONREC_contour($this->d,$this->y,$this->x,$this->nc);

		return $this->contours;
	}

	public function toWKT($type = null) {
		if(empty($this->contours))
			return false;

		try {
			$cons = array();
			for ($i=0; $i<count($this->contours);$i++) {
				//
				$cval = $this->contours[$i]['value'];
				$segments = $this->contours[$i]['segments'];

				if(count($segments)<2)
					throw new \Exception("No segments found!");

				// Make WKT string
				$coords = array();
				//$coords[] = "{$segments[0]['y1']} {$segments[0]['x1']}"; // Lon,Lat
				for ($j=0;$j<count($segments);$j++) {
					if($type === "MULTILINESTRING") {
						$coords[] = "({$segments[$j]['y1']} {$segments[$j]['x1']}, {$segments[$j]['y2']} {$segments[$j]['x2']})";
					} else {					
						$coords[] = "{$segments[$j]['y1']} {$segments[$j]['x1']}, {$segments[$j]['y2']} {$segments[$j]['x2']}";
						//$coords[] = "{$segments[$j]['y2']} {$segments[$j]['x2']}"; // Lon,Lat
					}
				}
				if(empty($type) || $type === "POLYGON") {
					// Add the first coords for close the polygon
					$coords[] = "{$segments[0]['y1']} {$segments[0]['x1']}";
				}

				// Make the WKT string
				if ($type === "LINESTRING") {
					$wkt = $this->linStart.implode(",",$coords).$this->linEnd;
				} else if ($type === "MULTILINESTRING") {
					$wkt = $this->multilinStart.implode(",",$coords).$this->multilinEnd;
				} else {
					$wkt = $this->polStart.implode(",",$coords).$this->polEnd;
				}

				// Out
				$cons[] = array('value' => $cval, 'wkt' => $wkt);
			}
			return $cons;
		} catch (\Exception $ex) {
			throw $ex;
		}
	}
}