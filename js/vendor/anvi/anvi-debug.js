/**
 * @copyright AnVi 2015
 */

goog.provide('anvi');

goog.require('goog.dom.classes');

// Dependencies
goog.addDependency('../../anvi/src/control/anvi.control.navbar.js', ['anvi.control.Navbar'], []);
goog.addDependency('../../anvi/src/control/anvi.control.toolbar.js', ['anvi.control.Toolbar'], []);
goog.addDependency('../../anvi/src/control/anvi.control.navbaritem.js', ['anvi.control.NavbarItem'], []);
goog.addDependency('../../anvi/src/control/anvi.control.menu.js', ['anvi.control.Menu'], []);
goog.addDependency('../../anvi/src/control/anvi.control.slider.js', ['anvi.control.Slider'], []);
goog.addDependency('../../anvi/src/control/anvi.control.select.js', ['anvi.control.Select'], []);
goog.addDependency('../../anvi/src/field/anvi.field.isocontour.js', ['anvi.field.Isocontour'], []);
goog.addDependency('../../anvi/src/field/anvi.field.wind.js', ['anvi.field.Wind'], []);
goog.addDependency('../../anvi/src/field/anvi.field.image.js', ['anvi.field.Image'], []);
goog.addDependency('../../anvi/src/layer/anvi.layer.symbol.js', ['anvi.layer.Symbol'], []);
goog.addDependency('../../anvi/src/layer/anvi.layer.path.js', ['anvi.layer.Path'], []);
goog.addDependency('../../anvi/src/gis/anvi.gis.measure.js', ['anvi.gis.Measure'], []);

console.log('Dependencies were setup!');

if(!ol) {
	console.error('Cannot found the ol.js');
	exit;
}

// Define anvi properties
anvi.Debug = true;

/**
 * @type {ol.Map}
 */
anvi.map = new ol.Map({
	view: new ol.View({
  	center: [0, 0],
  	zoom: 3
	}),
  layers: [
    new ol.layer.Tile({
      source: 
        /*new ol.source.XYZ({
          url: '//mt0.googleapis.com/vt/x={x}&y={y}&z={z}'
        })*/
        new ol.source.MapQuest({layer: 'osm'})
    })
	],
	target: 'map'
});

// Path
anvi.root = document.currentScript.src.slice(0,-("anvi-dubug.js".length));

// Interactions
anvi.map.dragAndDropInteraction = new ol.interaction.DragAndDrop({
  formatConstructors: [
    ol.format.GPX,
    ol.format.GeoJSON,
    ol.format.IGC,
    ol.format.KML,
    ol.format.WKT,
    ol.format.TopoJSON
  ]
});

anvi.helpers = {
  pointermoveListener: undefined,
  drawingType: undefined,
  draw: undefined,

  /**
   * Helper function to send a binary file in server
   */
  showOnlyLayer: function (layer) {
    anvi.map.getLayers().forEach( function (lay) {
      if (lay.get('id') === layer.get('id')) {
        layer.setVisible(true);
      } else if (
        lay instanceof ol.layer.Image || lay instanceof ol.layer.Vector) {
        lay.setVisible(false);
      }
    });
  },

  /**
   * Helper function to send data files in server
   */
  sendDataFile: function (opts) {
    var _baseUrl = opts.baseUrl || anvi.dataUrl || undefined;
    var _file = opts.file || undefined;
    var _url = _baseUrl;
    var _type = opts.type;

    if (_file) {
      var tmp = _type.split(":");
      if(tmp.length < 1 || tmp.length > 2) {
        swal('Error in the format of file type','Right format: [file_type:file_variable]','error');
        return false;
      }
      _type = tmp[0];
      _var = tmp[1] || undefined;

      // Add file to form
      var contents = new FormData();
      contents.append(_type, _file);

      // Make url
      if (_var) {
        _url += _type + '/' + _var;
      } else {
        _url += _type;
      }
      console.log('url: ' + _url);

      // Call service for saving data in DB
      // -----------------------------------
      goog.net.XhrIo.send(
        _url,
        function(e){
          var xhr = e.target;

          if(xhr.getLastErrorCode() != goog.net.ErrorCode.NO_ERROR) {
            swal(xhr.getLastError(), "Perhap the data have already existed in DB.", "error");
            throw xhr.getLastError();
          }

          // Get the response object
          var resp = xhr.getResponseJson();
          if (parseInt(resp.error)) {
            swal("Cannot upload the file: " + _file.name, resp.message, "error");
            throw resp.message;
          } else {
            //swal("File has been uploaded", resp.message, "success");
            console.log('File '+ _file.name + ' has been uploaded. ' + resp.message);
          }
        },
        "POST",
        contents
      );
      // ------------------------------------//*/
    }
  },

  /**
   * Helper function to send a text file in server
   */
  sendTextFile: function (opts) {
    var _baseUrl = opts.baseUrl || anvi.dataUrl  || undefined;
    var _file = opts.file || undefined;
    var _type = opts.type || "";

    var _var = undefined;
    var _url = _baseUrl;

    if (_file) {
      // Text file
      var r = new FileReader();
      r.readAsText(_file);

      // Text file
      // Read only txt file and image file at client side
      // Put the content to the server side as string
      r.onload = function(e) {
        var contents = e.target.result;
        var contentType = "";
        var sign = contents.substr(0, 10);

        if(!_type) {
          if(sign.match(/diamond/g)) {
            contentType = "text/plain";

            // For drag and drop
            if(sign.match(/diamond 3/g)) {
              _type = "rain";
            } else if (sign.match(/diamond 11/g)) {
              _type = "ec";
            } else if (sign.match(/diamond 4/g)) {
              _type = "ground";
            } else if (sign.match(/diamond 2/g)) {
              _type = "plot";
            }

          } else {
            // Show dialog for choose the file format
            alert('The sign ' + sign + ' is not valid!');
            console.error('The sign ' + sign + ' is not valid!');
            return false;
          }
        } else {
          var tmp = _type.split(":");
          if(tmp.length != 2) {
            alert('Error in the format of file type [file_type:file_variable]');
            console.error('Error in the format of file type [file_type:file_variable]');
            return false;
          }
          _type = tmp[0];
          _var = tmp[1] || undefined;
        }

        if (_var) {
          _url += _type + '/' + _var;
        } else {
          _url += _type;
        }

        console.log('url: ' + _url);

        // Call service for saving data in DB
        // -----------------------------------
        goog.net.XhrIo.send(
          _url,
          function(e){
            var xhr = e.target;

            if(xhr.getLastErrorCode() != goog.net.ErrorCode.NO_ERROR) {
              console.error(xhr.getLastError());
              alert(xhr.getLastError());
            }

            // Get the response object
            var resp = xhr.getResponseJson();
            
            if (parseInt(resp.error)) {
              swal(resp.message, resp.error, "error");
            }
          },
          "POST",
          contents,
          {'Content-type': contentType}
        );
        // ------------------------------------//*/
      }
      
    }
  },

  activateGIS: function (options) {
    // Disable click events

    var opts = options || {};
    if(opts.activated == false || opts.type != anvi.helpers.drawingType) {
      anvi.map.removeInteraction(anvi.helpers.draw);

      // Unset event pointermove
      if (anvi.helpers.pointermoveListener) {
        anvi.map.unByKey(anvi.helpers.pointermoveListener);
        anvi.helpers.pointermoveListener = null;
      }

      if (opts.activated == false) {
        return;
      }
    }

    // Save the drawing type
    anvi.helpers.drawingType = opts.type;

    var wgs84Sphere = new ol.Sphere(6378137);

    if (!anvi.helpers.drawingLayer) {
      anvi.helpers.drawingLayer = new ol.layer.Vector({
        source: new ol.source.Vector(),
        style: new ol.style.Style({
          fill: new ol.style.Fill({
            color: 'rgba(255, 255, 255, 0.2)'
          }),
          stroke: new ol.style.Stroke({
            color: '#ffcc33',
            width: 2
          }),
          image: new ol.style.Circle({
            radius: 7,
            fill: new ol.style.Fill({
              color: '#ffcc33'
            })
          })
        })
      });
      anvi.map.addLayer(anvi.helpers.drawingLayer);
    }

    /**
     * Currently drawn feature.
     * @type {ol.Feature}
     */
    var sketch;


    /**
     * The help tooltip element.
     * @type {Element}
     */
    var helpTooltipElement;


    /**
     * Overlay to show the help messages.
     * @type {ol.Overlay}
     */
    var helpTooltip;


    /**
     * The measure tooltip element.
     * @type {Element}
     */
    var measureTooltipElement;

    /**
     * Overlay to show the measurement.
     * @type {ol.Overlay}
     */
    var measureTooltip;


    /**
     * Message to show when the user is drawing a polygon.
     * @type {string}
     */
    var continuePolygonMsg = 'Click to continue drawing the polygon';


    /**
     * Message to show when the user is drawing a line.
     * @type {string}
     */
    var continueLineMsg = 'Click to continue drawing the line';

    /**
     * Handle pointer move.
     * @param {ol.MapBrowserEvent} evt The event.
     */
    var pointerMoveHandler = function(evt) {
      if (evt.dragging) {
        return;
      }
      /** @type {string} */
      var helpMsg = 'Click to start drawing';

      if (sketch) {
        var geom = (sketch.getGeometry());
        if (geom instanceof ol.geom.Polygon) {
          helpMsg = continuePolygonMsg;
        } else if (geom instanceof ol.geom.LineString) {
          helpMsg = continueLineMsg;
        }
      }

      helpTooltipElement.innerHTML = helpMsg;
      if (helpTooltip) {
        helpTooltip.setPosition(evt.coordinate);
      }

      goog.dom.classes.remove(helpTooltipElement,'hidden');
    };

    anvi.helpers.pointermoveListener = anvi.map.on('pointermove', pointerMoveHandler);

    anvi.map.getViewport().onmouseout = function() {
      goog.dom.classes.add(helpTooltipElement,'hidden');
    };

    var type_ = opts.type ? opts.type : 'area';
    var geodesic_ = opts.geodesic ? opts.geodesic : true;
    //var draw = this.draw; // global so we can remove it later


    /**
     * Format length output.
     * @param {ol.geom.LineString} line The line.
     * @return {string} The formatted length.
     */
    var formatLength = function(line) {
      var length;
      if (geodesic_) {
        var coordinates = line.getCoordinates();
        length = 0;
        var sourceProj = anvi.map.getView().getProjection();
        for (var i = 0, ii = coordinates.length - 1; i < ii; ++i) {
          var c1 = ol.proj.transform(coordinates[i], sourceProj, 'EPSG:4326');
          var c2 = ol.proj.transform(coordinates[i + 1], sourceProj, 'EPSG:4326');
          length += wgs84Sphere.haversineDistance(c1, c2);
        }
      } else {
        length = Math.round(line.getLength() * 100) / 100;
      }
      var output;
      if (length > 100) {
        output = (Math.round(length / 1000 * 100) / 100) +
            ' ' + 'km';
      } else {
        output = (Math.round(length * 100) / 100) +
            ' ' + 'm';
      }
      return output;
    };


    /**
     * Format area output.
     * @param {ol.geom.Polygon} polygon The polygon.
     * @return {string} Formatted area.
     */
    var formatArea = function(polygon) {
      var area;
      if (geodesic_) {
        var sourceProj = anvi.map.getView().getProjection();
        var geom = /** @type {ol.geom.Polygon} */(polygon.clone().transform(
            sourceProj, 'EPSG:4326'));
        var coordinates = geom.getLinearRing(0).getCoordinates();
        area = Math.abs(wgs84Sphere.geodesicArea(coordinates));
      } else {
        area = polygon.getArea();
      }
      var output;
      if (area > 10000) {
        output = (Math.round(area / 1000000 * 100) / 100) +
            ' ' + 'km<sup>2</sup>';
      } else {
        output = (Math.round(area * 100) / 100) +
            ' ' + 'm<sup>2</sup>';
      }
      return output;
    };


    function addInteraction() {
      var type = (type_ == 'area' ? 'Polygon' : 'LineString');
      anvi.helpers.draw = new ol.interaction.Draw({
        source: anvi.helpers.drawingLayer.getSource(),
        type: /** @type {ol.geom.GeometryType} */ (type),
        style: new ol.style.Style({
          fill: new ol.style.Fill({
            color: 'rgba(255, 255, 255, 0.2)'
          }),
          stroke: new ol.style.Stroke({
            color: 'rgba(0, 0, 0, 0.5)',
            lineDash: [10, 10],
            width: 2
          }),
          image: new ol.style.Circle({
            radius: 5,
            stroke: new ol.style.Stroke({
              color: 'rgba(0, 0, 0, 0.7)'
            }),
            fill: new ol.style.Fill({
              color: 'rgba(255, 255, 255, 0.2)'
            })
          })
        })
      });
      anvi.map.addInteraction(anvi.helpers.draw);

      createMeasureTooltip();
      createHelpTooltip();

      var listener;
      anvi.helpers.draw.on('drawstart',
        function(evt) {
          // set sketch
          sketch = evt.feature;

          /** @type {ol.Coordinate|undefined} */
          var tooltipCoord = evt.coordinate;

          listener = sketch.getGeometry().on('change', function(evt) {
            var geom = evt.target;
            var output;
            if (geom instanceof ol.geom.Polygon) {
              output = formatArea(geom);
              tooltipCoord = geom.getInteriorPoint().getCoordinates();
            } else if (geom instanceof ol.geom.LineString) {
              output = formatLength(geom);
              tooltipCoord = geom.getLastCoordinate();
            }
            measureTooltipElement.innerHTML = output;
            measureTooltip.setPosition(tooltipCoord);
          });
        }, this
      );

      anvi.helpers.draw.on('drawend',
        function(evt) {
          measureTooltipElement.className = 'tooltip tooltip-static';
          measureTooltip.setOffset([0, -7]);
          // unset sketch
          sketch = null;
          // unset tooltip so that a new one can be created
          measureTooltipElement = null;
          createMeasureTooltip();
          ol.Observable.unByKey(listener);
        }, this
      );
    }


    /**
     * Creates a new help tooltip
     */
    function createHelpTooltip() {
      if (helpTooltipElement) {
        helpTooltipElement.parentNode.removeChild(helpTooltipElement);
      }
      helpTooltipElement = document.createElement('div');
      helpTooltipElement.className = 'tooltip hidden';
      helpTooltip = new ol.Overlay({
        element: helpTooltipElement,
        offset: [15, 0],
        positioning: 'center-left'
      });
      anvi.map.addOverlay(helpTooltip);
    }


    /**
     * Creates a new measure tooltip
     */
    function createMeasureTooltip() {
      if (measureTooltipElement) {
        measureTooltipElement.parentNode.removeChild(measureTooltipElement);
      }
      measureTooltipElement = document.createElement('div');
      measureTooltipElement.className = 'tooltip tooltip-measure';
      measureTooltip = new ol.Overlay({
        element: measureTooltipElement,
        offset: [0, -15],
        positioning: 'bottom-center'
      });
      anvi.map.addOverlay(measureTooltip);
    }


    /**
     * Let user change the geometry type.
     *
    typeSelect.onchange = function() {
      anvi.map.removeInteraction(draw);
      addInteraction();
    };//*/

    addInteraction();
  }
}

/*
var styleFunction = function(feature, resolution) {
  var featureStyleFunction = feature.getStyleFunction();
  if (featureStyleFunction) {
    return featureStyleFunction.call(feature, resolution);
  } else {
    return defaultStyle[feature.getGeometry().getType()];
  }
};*/
