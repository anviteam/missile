goog.provide('anvi.control.Select');

goog.require('goog.style');
goog.require('goog.math');
goog.require('goog.math.Rect');
goog.require('goog.asserts');
goog.require('goog.dom');
goog.require('goog.dom.TagName');
goog.require('goog.dom.appendChild');
goog.require('goog.events');
goog.require('goog.events.Event');
goog.require('goog.events.EventType');

/**
 * @classdesc
 * Control for rendering a select
 *
 * @constructor
 * @extends {ol.control.Control}
 * @param {Object=} opt_options Control options.
 * @api stable
 */
anvi.control.Select = function(opt_options) {

    var options = goog.isDef(opt_options) ? opt_options : {};

    var pos = goog.isDef(options.position) ?
        options.position : 'right';
    var className = goog.isDef(options.className) ?
        options.className : 'anvi-control-select';
    this.baseUrl_ = goog.isDef(options.baseUrl) ?
        options.baseUrl : undefined;

    // Select element
    this.selElm_ = goog.dom.createDom(goog.dom.TagName.SELECT,
        ['anvi-select', 'ol-unselectable']);

    // Container
    var container = goog.dom.createDom(goog.dom.TagName.DIV,
        [className, 'anvi-' + pos, 'ol-unselectable', 'ol-control'],
        [this.selElm_]);

    goog.base(this, {
        element: container,
        target: options.target
    });
};
goog.inherits(anvi.control.Select, ol.control.Control);

/**
 * Add an option.
 *
 * @param {string} label The label.
 * @param {string} value The value.
 * @public
 */
anvi.control.Select.prototype.addOption = function(label, value) {
	var optElm = goog.dom.createDom(goog.dom.TagName.OPTION,
        ['anvi-option']);
	optElm.setAttribute('value', value);
	optElm.innerHTML = label;

	goog.dom.appendChild(this.selElm_,optElm);
}

/**
 * Get the current option.
 *
 * @public
 */
anvi.control.Select.prototype.getCurrentValue = function() {
    var currentOption = this.selElm_.options[this.selElm_.selectedIndex];
    if (currentOption)
        return currentOption.value;
    else 
        return false;
}

/**
 * Get the time data from DB
 *
 * @public
 */
anvi.control.Select.prototype.reload = function() {
    if (!this.baseUrl_) return false;

    var url_ = this.baseUrl_ + 'times';

    var me =this;
    goog.net.XhrIo.send(
        url_,
        function(e){
            var xhr = e.target;

            if(xhr.getLastErrorCode() != goog.net.ErrorCode.NO_ERROR) {
                console.error(xhr.getLastError());
                alert('Cannot load the time sequences from DB.');
            }

            // Get the response object
            try {
                var _data = xhr.getResponseJson();
                
                for (var i in _data) {
                    me.addOption(_data[i],_data[i]);
                }
            } catch (err) {
                throw err;
            }
        },
        "GET"
    );
}