goog.provide('anvi.control.NavbarItem');

goog.require('goog.events.Event');

goog.require('anvi.field.Isocontour');
goog.require('anvi.field.Image');
goog.require('anvi.field.Wind');
goog.require('anvi.layer.Symbol');
goog.require('anvi.layer.Path');

/**
 * @classdesc
 * Control for rendering navigation bar item
 *
 * @constructor
 * @param {Object=} opt_options Control options.
 * @api stable
 */
anvi.control.NavbarItem = function(opt_options, navbar, etarget = null) {
    var options = opt_options || {};

    var _active = options.active ? 'anvi-active' : undefined;
    var _parent = options.parent || false;
    
    var _click = (typeof options.onClick === "function") ? options.onClick : undefined;
    var _tooggle = false;

    this._stats = options.stats || undefined;
    this._url = options.baseurl || undefined;
    this._datatype = options.datatype || undefined;
    this._variable = options.variable || undefined;

    this._type = options.type || undefined;
    this._time = options.time || undefined;
    this._pressure = options.pressure || undefined;
    this._name = options.name || 'noname';
    this._id = this._name;
    if (_parent) {
    	this._id = _parent.name() + '-' + this._name;
    }

    // Define the navbar item
    this.navbar_item = document.createElement('li');
    this.navbar_item.className = "anvi-control-navbar-item";
    if(options.width) {
        goog.style.setSize(this.navbar_item, options.width, options.height || '1.375em');
    }

    var navbar_item_link = document.createElement('a');
    navbar_item_link.href = "#";
    navbar_item_link.innerHTML = this._name;
    this.navbar_item.appendChild(navbar_item_link);

    etarget.addEventListener('change:active', function (e) {
        if (e.target.src !== navbar_item_link.innerHTML) {
            goog.dom.classes.remove(navbar_item_link,_active);
            _tooggle = false;
            this._tooggle = _tooggle;
        }
    }, this);

    // Click event
    if( _click ) {
        navbar_item_link.addEventListener('click', function(e) {
            _tooggle = !_tooggle;
            if(_active && _tooggle) {
                navbar_item_link.className = _active;
            } else if (_active) {
                goog.dom.classes.remove(navbar_item_link,_active);
            }
            _click.call(this, _tooggle);

            // Fire change:active event
            var evt = new goog.events.Event('change:active', { src: this.innerHTML, tooggle: false });
            goog.events.dispatchEvent(etarget,evt);

            this._tooggle = _tooggle;

        }, false);
        navbar_item_link.addEventListener('touch', _click, false);
    } else {
        // Defaut show data click
        var me = this;
        if(this._url) {
            goog.events.listen(navbar_item_link,
                goog.events.EventType.CLICK, 
                goog.partial(me.showData_),
                false, this);
        }
    }

    // If has parent
    if(_parent) {

    	var parentElm = _parent.htmlElement();

        // Get the dropdown content if exists
        var _dropdown = parentElm.querySelector(".anvi-dropdown-content");
        if(!_dropdown) {
            parentElm.className += " anvi-dropdown";

            _dropdown = document.createElement('ul');
            _dropdown.className = "anvi-dropdown-content";
        }
        
        // Add the new item into the dropdown content
        _dropdown.appendChild(this.navbar_item);

        // Add the content into the parent
        parentElm.appendChild(_dropdown);
    } else {
        // Add the item into the navbar
        navbar.appendChild(this.navbar_item);
    }

}

/**
 * @public
 */
anvi.control.NavbarItem.prototype.name = function () {
	return this._name;
}

/**
 * @public
 */
anvi.control.NavbarItem.prototype.tooggle = function () {
    return this._tooggle;
}

/**
 * @public
 */
anvi.control.NavbarItem.prototype.htmlElement = function () {
	return this.navbar_item;
}

/**
 * @param {string} url New url string.
 * @public
 */
anvi.control.NavbarItem.prototype.setTime = function (time) {
	this._time = time;
}

/**
 * @param {string} type Data type.
 * @private
 */
anvi.control.NavbarItem.prototype.showData_ = function (e) {
    e.preventDefault();

    // Show loading icon
    var loadingElm = document.getElementById("loading");
    loadingElm.style.display = "block";

    // Add layer to map
    ///////////////////////////////////
    var url = this._url + this._datatype;
    if (this._variable) {
        url += '/' + this._variable;
    }
    var id = this._id;
    if (this._time) {
    	url += '/' + this._time;
    	id = this._id + '-' + this._time;

        if (this._pressure) {
            url += '/' + this._pressure;
            id += '-' + this._pressure;
        }
    }
console.log('url: ' + url);

    var urlPlot = this._url + 'plot/' + this._variable;
    if (this._time) {
        urlPlot += '/' + this._time;

        if (this._pressure) {
            urlPlot += '/' + this._pressure;
        }
    }
console.log('urlPlot: ' + urlPlot);
    try {
        // Create a data layer based on type
        var dataLayer;
        if (this._type === "isocontour") {
            dataLayer = new anvi.field.Isocontour({
                url: url,
                stats: this._stats
            });
        } else if (this._type === "wind") {
            dataLayer = new anvi.field.Wind({
                url: url,
                stats: this._stats
            });
        } else if (this._type === "path") {
            dataLayer = new anvi.layer.Path({
                url: url,
                stats: this._stats
            });
        } else if (this._type === "image" || typeof this._type == "undefined") {
            dataLayer = new anvi.field.Image({
                url: url,
                opacity: 0.6
            });
        }
        dataLayer.set('type', this._type || 'image');
        dataLayer.set('id', id || 'noid');
        // Add to map
        anvi.map.addLayer(dataLayer);

        // Show plot layer
        if (this._type != 'image' && this._type != 'wind' && this._type != 'path') {
            var plotLayer = new anvi.layer.Symbol({
                url: urlPlot
            });
            plotLayer.set('id', id || 'noid');
            anvi.map.addLayer(plotLayer);
        }

        // Show only this layer
        anvi.helpers.showOnlyLayer(dataLayer);
    } catch (err) {
        loadingElm.style.display = "none";
        console.log(err);
        alert(err.name + '\r\n' + err.message);
        throw err;
    }

    // Zoom on layer
    /*
    dataLayer.addEventListener('change:source', function () {
        loadingElm.style.display = "none";
        //anvi.map.getView().fit(weatherLayer.getSource().getSource().getExtent(), anvi.map.getSize());
    }, false);*/

    if(this._type && (this._type != "image" && this._type != "path") ) {
        dataLayer.getSource().getSource().addEventListener('change', function () {
            loadingElm.style.display = "none";
            //anvi.map.getView().fit(weatherLayer.getSource().getSource().getExtent(), anvi.map.getSize());
        }, false);
    } else if (this._type === "path") {
        loadingElm.style.display = "none";
    } else {
        dataLayer.addEventListener('change:source', function () {
            loadingElm.style.display = "none";
            //anvi.map.getView().fit(weatherLayer.getSource().getSource().getExtent(), anvi.map.getSize());
        }, false);
    }//*/
    
}