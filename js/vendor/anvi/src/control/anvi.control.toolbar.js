//
//

// .\js\vendor\anvi\src\control

goog.provide('anvi.control.Toolbar');

goog.require('anvi.control.ShowData');

/**
 * @constructor
 * @extends {ol.control.Control}
 * @param {Object=} opt_options Control options.
 */
anvi.control.Toolbar = function(opt_options) {

  var options = opt_options || {};

  var this_ = this;
  this.element = document.createElement('div');
  this.element.className = 'anvi-toolbar ol-unselectable ol-control';
  //this.element.className = 'anvi-'+pos+' anvi-toolbar ol-unselectable ol-control';
  goog.style.setPosition(this_.element, options.left || '3.0em', options.top || '0.5em');

  goog.base (this, {
    element: this.element,
    target: options.target
  });

};
goog.inherits(anvi.control.Toolbar, ol.control.Control);

/**
 * Add button into the toolbar
 */
anvi.control.Toolbar.prototype.addButton = function (opt_options) {
  var options = opt_options || {};

  var button = new anvi.control.ShowData(options);
  //goog.style.setSize(but.button, options.width || '1.375em', options.height || '1.375em');

/*
  var board = document.createElement('ul');
  board.className = 'anvi-boardHide';
  button.appendChild(board);
  goog.style.setPosition(board, button.style.left);

  var menuitem = document.createElement('li');
  menuitem.className = 'ol-unselectable ol-control';
  menuitem.innerHTML = 'Pressure';
  board.appendChild(menuitem);
  menuitem = document.createElement('li');
  menuitem.className = 'ol-unselectable ol-control';
  menuitem.innerHTML = 'Temperature';
  board.appendChild(menuitem);

  var toggleBoard = function(e) {
      if (board.className === 'anvi-boardHide'){
          board.className = 'anvi-boardShow';
      }
      else {
          board.className = 'anvi-boardHide';
      }
  };

  button.addEventListener('click', toggleBoard, false);
  button.addEventListener('touchstart', toggleBoard, false);  */

  this.element.appendChild(button.element);
}