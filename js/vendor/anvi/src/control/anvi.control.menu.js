goog.provide('anvi.control.Menu');

/**
 * @classdesc
 * Control for rendering action buttons
 *
 * @constructor
 * @extends {ol.control.Control}
 * @param {Object=} opt_options Control options.
 * @api stable
 */
anvi.control.Menu = function(opt_options) {

    var options = opt_options || {};
    var boardElm = options.boardHTML || "";

    var button = document.createElement('button');
    button.style.cursor = 'pointer';
    button.innerHTML = options.label || 'M';
    goog.style.setSize(button, options.width || '1.375em', options.height || '1.375em');

    var this_ = this;

    var element = document.createElement('div');
    element.className = 'anvi-menu ol-unselectable ol-control';
    element.appendChild(button);
    goog.style.setPosition(element, options.left || '0.5em', options.top || '4.5em');

    this.boardDiv = document.createElement('div');
    this.boardDiv.className = 'anvi-boardDivHide';
    this.boardDiv.innerHTML = boardElm;
    element.appendChild(this.boardDiv);

    this.menuList = document.createElement('ul');
    this.boardDiv.appendChild(this.menuList);

    this.menuitem = document.createElement('li');
    this.menuitem.innerHTML = 'missile1';
    this.menuList.appendChild(this.menuitem);
    this.menuitem.addEventListener('click', function () {
        var url = "http://localhost:8000/missile/app/api.php?type=missile";
        goog.net.XhrIo.send(url,
        function(e){
            var xhr = e.target;

            if(xhr.getLastErrorCode() != goog.net.ErrorCode.NO_ERROR) {
              console.error(xhr.getLastError());
            }

            // Get the response object
            var obj = xhr.getResponseJson();
            
            // Create the layer
            var format = new ol.format.WKT();
            //console.log(obj[0].wkt);
            var feats = [];
            for (var i = 0; i < obj.length; i++) {
                var feat = format.readFeature(obj[i].wkt);
                feat.setProperties({'status': obj[i].status});
                feat.getGeometry().transform('EPSG:4326', 'EPSG:3857');
                feats.push(feat);
            };
            
            var missileLayer = new ol.layer.Vector({
                source: new ol.source.Vector({
                    features: feats
                }),
                style: function (feature) {
                    var status = feature.get('status');
                    
                    var clr = 'black';
                    if (status == 0) {
                      clr = 'blue';
                    } else if (status == 1) {
                      clr = 'red';
                    }

                    var style = new ol.style.Style({
                        image: new ol.style.Circle({
                            radius: 5,
                            fill: new ol.style.Fill({
                                color: clr
                            })
                        })
                    });
                    return [style];
                }
            });
            anvi.map.addLayer(missileLayer);
        },
        "GET");
    }, false);

    var toggleBoard = function(e) {
        if (this_.boardDiv.className === 'anvi-boardDivHide'){
            this_.boardDiv.className = 'anvi-boardDivShow';
        }
        else {
            this_.boardDiv.className = 'anvi-boardDivHide';
        }

    };

    button.addEventListener('click', toggleBoard, false);
    button.addEventListener('touchstart', toggleBoard, false);  

    ol.control.Control.call(this, {
        element: element,
        target: options.target
    });

};
ol.inherits(anvi.control.Menu, ol.control.Control);
