goog.provide('anvi.control.Slider');

goog.require('goog.style');
goog.require('goog.math');
goog.require('goog.math.Rect');
goog.require('goog.asserts');
goog.require('goog.dom');
goog.require('goog.dom.TagName');
goog.require('goog.events');
goog.require('goog.events.Event');
goog.require('goog.events.EventType');

/**
 * @classdesc
 * Control for rendering a slider
 *
 * @constructor
 * @extends {ol.control.Control}
 * @param {Object=} opt_options Control options.
 * @api stable
 */
anvi.control.Slider = function(opt_options) {

    var options = goog.isDef(opt_options) ? opt_options : {};

    this.min_ = options.min || 0;
    this.max_ = options.max || 100;
    this.step_ = options.step || 1;
    this.value_ = options.value || 0;
    this.orient_ = options.orient || 'horizontal';
    this.labels_ = options.labels || undefined;
    this.prefix_ = options.prefix || "";
    this.bubble_ = (options.bubble == undefined) ? true : options.bubble;

    /**
     * The direction of the slider. Will be determined from actual display of the
     * container and defaults to ol.control.ZoomSlider.direction.VERTICAL.
     *
     * @type {ol.control.Slider.direction}
     * @private
     */
    this.direction_ = this.orient_ === 'vertical' ? anvi.control.Slider.direction.VERTICAL
        : anvi.control.Slider.direction.HORIZONTAL;

    /**
     * Whether the slider is initialized.
     * @type {boolean}
     * @private
     */
    this.sliderInitialized_ = false;

    var pos = goog.isDef(options.position) ?
        options.position : 'right';
    var className = goog.isDef(options.className) ?
        options.className : 'anvi-slider';

    // Input element
    var inpElm = goog.dom.createDom(goog.dom.TagName.INPUT,
        [className + '-input', 'ol-unselectable']);
    inpElm.setAttribute('type','range');
    inpElm.setAttribute('orient', this.orient_);
    inpElm.setAttribute('min', this.min_);
    inpElm.setAttribute('max', this.max_);
    inpElm.setAttribute('step', this.step_);
    inpElm.setAttribute('value', this.value_);
    this.inpElm_ = inpElm;

    //Add tick marks to slider track    
    var ticks  = '<div class="sliderTickmarks "><span>0%</span></div>';
        ticks += '<div class="sliderTickmarks "><span>25%</span></div>';
        ticks += '<div class="sliderTickmarks "><span>50%</span></div>';
        ticks += '<div class="sliderTickmarks "><span>75%</span></div>';
        ticks += '<div class="sliderTickmarks "><span>100%</span></div>';

    // Bubble
    var bubbleElm;
    bubbleElm = goog.dom.createDom(goog.dom.TagName.INPUT,
        [className + '-' + 'bubble']);
    bubbleElm.setAttribute('type','text');
    if (this.direction_ == anvi.control.Slider.direction.VERTICAL) {
        goog.style.setSize(bubbleElm, '3.5em', '1.5em');
    } else {
        goog.style.setHeight(bubbleElm, '1.5em');
    }
    if (this.bubble_) {
        bubbleElm.style.display = 'block';
    } else {
        bubbleElm.style.display = 'none';
    }

    // Legend
    var legendElm;
    if(options.legend) {
        legendElm = goog.dom.createDom(goog.dom.TagName.LABEL,
            [className + '-' + 'legend']);
        if (this.direction_ == anvi.control.Slider.direction.VERTICAL) {
            goog.style.setPosition(legendElm, '-2px', '-1.5em');
        } else {
            goog.style.setPosition(legendElm, '-2.5em', '4px');
            if (options.legendOnclick) {
                legendElm.style.cursor = 'pointer';
                legendElm.addEventListener('click', options.legendOnclick, false);
            }
        }

        legendElm.innerHTML = options.legend;
    }

    // Container
    var container = goog.dom.createDom(goog.dom.TagName.DIV,
        [className + '-' + inpElm.getAttribute('orient'), 'anvi-' + pos, 'ol-unselectable', 'ol-control'],
        [bubbleElm,inpElm,legendElm]);

    bubbleElm.value = this.labels_ ? this.labels_[inpElm.value] : inpElm.value;
    goog.events.listen(inpElm,
        goog.events.EventType.CHANGE, 
        function () {
            goog.events.Event.stopPropagation;
            bubbleElm.value = this.labels_ ? this.labels_[inpElm.value] : inpElm.value;

            // Position
            this.setBubblePosition_(inpElm.value);
        },
        false, this);


    var render = goog.isDef(options.render) ?
        options.render : anvi.control.Slider.render;

    goog.base(this, {
        element: container,
        render: render,
        target: options.target
    });

};
goog.inherits(anvi.control.Slider, ol.control.Control);

/**
 * The enum for available directions.
 *
 * @enum {number}
 */
anvi.control.Slider.direction = {
  VERTICAL: 0,
  HORIZONTAL: 1
};

/**
 * Initializes the slider element. This will determine and set this controls
 * direction_ and also constrain the dragging of the thumb to always be within
 * the bounds of the container.
 *
 * @private
 */
anvi.control.Slider.prototype.initSlider_ = function() {
    var container = this.element;
    var containerSize = goog.style.getSize(container);

    var bubble = goog.dom.getFirstElementChild(container);
    var bubbleMargins = goog.style.getMarginBox(bubble);
    var bubbleBorderBoxSize = goog.style.getBorderBoxSize(bubble);
    var bubbleWidth = bubbleBorderBoxSize.width +
        bubbleMargins.right + bubbleMargins.left;
    var bubbleHeight = bubbleBorderBoxSize.height +
        bubbleMargins.top + bubbleMargins.bottom;
    this.bubbleSize_ = [bubbleWidth, bubbleHeight];

    var width = containerSize.width - bubbleWidth;
    var height = containerSize.height - bubbleHeight;

    var limits;
    if (containerSize.width > containerSize.height) {
        this.direction_ = anvi.control.Slider.direction.HORIZONTAL;
        limits = new goog.math.Rect(0, 0, width, 0);
    } else {
        this.direction_ = anvi.control.Slider.direction.VERTICAL;
        limits = new goog.math.Rect(0, 0, 0, height);
    }
    //this.dragger_.setLimits(limits);
    this.limits_ = limits;
    this.sliderInitialized_ = true;
};

anvi.control.Slider.prototype.value = function() {
    var val;
    if (this.labels_) {
        val = this.labels_[this.inpElm_.value];
    } else {
        val = parseInt(this.inpElm_.value);
    }
    return val;
}

/**
 * Positions the thumb inside its container according to the given value.
 *
 * @param {number} value The value.
 * @private
 */
anvi.control.Slider.prototype.setBubblePosition_ = function(value) {
    var position = this.getPositionForValue_(value);
    var bubble = goog.dom.getFirstElementChild(this.element);
    var bubbleSize = goog.style.getSize(bubble);

    if (this.direction_ == anvi.control.Slider.direction.HORIZONTAL) {
        var left = this.limits_.left + this.limits_.width * position;
        goog.style.setPosition(bubble, left, this.limits_.top - bubbleSize.height);
    } else {
        //var top = this.limits_.top + this.limits_.height * position;
        var top = this.limits_.height - this.limits_.height * position;
        goog.style.setPosition(bubble, this.limits_.left - bubbleSize.width, top);
    }
};

anvi.control.Slider.prototype.getPositionForValue_ = function(value) {
    var pos_ = ( (value - this.min_)/(this.max_ - this.min_) );
    return pos_;
}

/**
 * Update the slider element.
 * @param {ol.MapEvent} mapEvent Map event.
 * @this {ol.control.ZoomSlider}
 * @api
 */
anvi.control.Slider.render = function(mapEvent) {
    if (goog.isNull(mapEvent.frameState)) {
        return;
    }
    goog.asserts.assert(goog.isDefAndNotNull(mapEvent.frameState.viewState),
      'viewState should be defined');
    if (!this.sliderInitialized_) {
        this.initSlider_();
    }
    var res = 0;
    //if (res !== this.currentValue_) {
    //    this.currentValue_ = res;
        this.setBubblePosition_(res);
    //}
};