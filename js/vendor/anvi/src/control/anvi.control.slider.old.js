goog.provide('anvi.control.Slider');

goog.require('goog.asserts');
goog.require('goog.dom');
goog.require('goog.dom.TagName');
goog.require('goog.events');
goog.require('goog.events.Event');
goog.require('goog.events.EventType');
goog.require('goog.fx.DragEvent');
goog.require('goog.fx.Dragger');
goog.require('goog.fx.Dragger.EventType');
goog.require('goog.math');
goog.require('goog.math.Rect');
goog.require('goog.style');


/**
 * @classdesc
 * Control for rendering a slider
 *
 * @constructor
 * @extends {ol.control.Control}
 * @param {Object=} opt_options Control options.
 * @api stable
 */
anvi.control.Slider = function(opt_options) {

    var options = goog.isDef(opt_options) ? opt_options : {};

    this.min_ = options.min || 0;
    this.max_ = options.max || 1;
    this.interval_ = options.interval || 0.1;
    this.labels_ = options.labels || undefined;
    this.prefix_ = options.prefix || "";

    /**
     * The direction of the slider. Will be determined from actual display of the
     * container and defaults to ol.control.ZoomSlider.direction.VERTICAL.
     *
     * @type {ol.control.Slider.direction}
     * @private
     */
    this.direction_ = anvi.control.Slider.direction.VERTICAL;

    /**
     * The calculated thumb size (border box plus margins).  Set when initSlider_
     * is called.
     * @type {ol.Size}
     * @private
     */
    this.thumbSize_ = null;

    /**
     * Whether the slider is initialized.
     * @type {boolean}
     * @private
     */
    this.sliderInitialized_ = false;

    /**
     * @private
     * @type {number}
     */
    this.duration_ = goog.isDef(options.duration) ? options.duration : 200;

    var pos = goog.isDef(options.position) ?
        options.position : 'right';
    var className = goog.isDef(options.className) ?
        options.className : 'anvi-slider';
    var thumbElement = goog.dom.createDom(goog.dom.TagName.DIV,
        [className + '-thumb', 'ol-unselectable']);
    this.labelElement_ = goog.dom.createDom(goog.dom.TagName.DIV,
        [className + '-label' + '-'+pos]);
    if(this.labels_) {
        this.labelElement_.innerText = this.prefix_ + this.labels_[0];
        //this.currentValue_ = this.labels_[0];
        this.currentLabel_ = this.labels_[0];
    } else {
        this.labelElement_.innerText = this.prefix_ + this.min_;
        //this.currentValue_ = this.min_;
        this.currentLabel_ = this.min_;
    }
    this.currentValue_ = this.min_;

    var grad = goog.dom.createDom(goog.dom.TagName.LABEL,
        [className + '-graduation', 'ol-unselectable']);
    grad.setAttribute('data-value',1);
    grad.innerText = '1000';

    var containerElement = goog.dom.createDom(goog.dom.TagName.DIV,
        [className + '-'+pos, 'ol-unselectable', 'ol-control'],
        [thumbElement,this.labelElement_, grad]);

    /**
    * @type {goog.fx.Dragger}
    * @private
    */
    this.dragger_ = new goog.fx.Dragger(thumbElement);
    //this.registerDisposable(this.dragger_);

    goog.events.listen(this.dragger_, goog.fx.Dragger.EventType.START,
      this.handleDraggerStart_, false, this);
    goog.events.listen(this.dragger_, goog.fx.Dragger.EventType.DRAG,
      this.handleDraggerDrag_, false, this);
    goog.events.listen(this.dragger_, goog.fx.Dragger.EventType.END,
      this.handleDraggerEnd_, false, this);

    //goog.events.listen(containerElement, goog.events.EventType.CLICK,
    //  this.handleContainerClick_, false, this);
    goog.events.listen(thumbElement, goog.events.EventType.CLICK,
      goog.events.Event.stopPropagation);

    var render = goog.isDef(options.render) ?
        options.render : anvi.control.Slider.render;

    goog.base(this, {
        element: containerElement,
        render: render
    });

    this.set('value',this.currentValue_);

};
goog.inherits(anvi.control.Slider, ol.control.Control);

/**
 * The enum for available directions.
 *
 * @enum {number}
 */
anvi.control.Slider.direction = {
  VERTICAL: 0,
  HORIZONTAL: 1
};


/**
 * @inheritDoc
 */
anvi.control.Slider.prototype.setMap = function(map) {
    goog.base(this, 'setMap', map);
    if (!goog.isNull(map)) {
        map.render();
    }
};

/**
 * Handle dragger start events.
 * @param {goog.fx.DragEvent} event The drag event.
 * @private
 */
anvi.control.Slider.prototype.handleDraggerStart_ = function(event) {
    //this.getMap().getView().setHint(ol.ViewHint.INTERACTING, 1);
    //console.log('Start drag!');
};


/**
 * Handle dragger drag events.
 *
 * @param {goog.fx.DragEvent} event The drag event.
 * @private
 */
anvi.control.Slider.prototype.handleDraggerDrag_ = function(event) {
    var relativePosition = this.getRelativePosition_(event.left, event.top);
    var res_ = (this.max_ - this.min_)/this.interval_;

    this.currentLabel_ = this.getValueForPosition_(relativePosition, res_);
    this.labelElement_.innerText = this.prefix_ + this.currentLabel_;
    //console.log('Current value: ' + this.currentValue_);
};

/**
 * Get the current value.
 *
 */
anvi.control.Slider.prototype.getCurrentLabel = function () {
    return this.currentLabel_;
}

/**
 * Handle dragger end events.
 * @param {goog.fx.DragEvent} event The drag event.
 * @private
 */
anvi.control.Slider.prototype.handleDraggerEnd_ = function(event) {
    var map = this.getMap();
    var view = map.getView();
    //view.setHint(ol.ViewHint.INTERACTING, -1);

    this.set('value', this.currentValue_);
};

/**
 * Calculates the corresponding resolution of the thumb given its relative
 * position (where 0 is the minimum and 1 is the maximum).
 *
 * @param {number} position The relative position of the thumb.
 * @return {number} resolution The corresponding resolution.
 * @private
 */
anvi.control.Slider.prototype.getValueForPosition_ = function(position, resolution) {
    var val = parseInt(position*resolution) + this.min_;
    if( this.labels_
        && this.labels_.length == 1+parseInt((this.max_-this.min_)/this.interval_)
        ) {
        val = this.labels_[val];
    }
    this.currentValue_ = position;
    return val;
};

/**
 * Determines the relative position of the slider for the given resolution.  A
 * relative position of 0 corresponds to the minimum view resolution.  A
 * relative position of 1 corresponds to the maximum view resolution.
 *
 * @param {number} res The resolution.
 * @return {number} The relative position value (between 0 and 1).
 * @private
 */
anvi.control.Slider.prototype.getPositionForValue_ = function(res) {
    return res;
};

/**
 * Positions the thumb inside its container according to the given resolution.
 *
 * @param {number} res The res.
 * @private
 */
anvi.control.Slider.prototype.setThumbPosition_ = function(res) {
    var position = this.getPositionForValue_(res);
    var dragger = this.dragger_;
    var thumb = goog.dom.getFirstElementChild(this.element);

    if (this.direction_ == anvi.control.Slider.direction.HORIZONTAL) {
        var left = dragger.limits.left + dragger.limits.width * position;
        goog.style.setPosition(thumb, left);
    } else {
        var top = dragger.limits.top + dragger.limits.height * position;
        goog.style.setPosition(thumb, dragger.limits.left, top);
    }
};

/**
 * Calculates the relative position of the thumb given x and y offsets.  The
 * relative position scales from 0 to 1.  The x and y offsets are assumed to be
 * in pixel units within the dragger limits.
 *
 * @param {number} x Pixel position relative to the left of the slider.
 * @param {number} y Pixel position relative to the top of the slider.
 * @return {number} The relative position of the thumb.
 * @private
 */
anvi.control.Slider.prototype.getRelativePosition_ = function(x, y) {
    var draggerLimits = this.dragger_.limits;
    var amount;
    if (this.direction_ === anvi.control.Slider.direction.HORIZONTAL) {
        amount = (x - draggerLimits.left) / draggerLimits.width;
    } else {
        amount = (y - draggerLimits.top) / draggerLimits.height;
    }

    return goog.math.clamp(amount, 0, 1);
};

/**
 * Initializes the slider element. This will determine and set this controls
 * direction_ and also constrain the dragging of the thumb to always be within
 * the bounds of the container.
 *
 * @private
 */
anvi.control.Slider.prototype.initSlider_ = function() {
    var container = this.element;
    var containerSize = goog.style.getSize(container);

    var thumb = goog.dom.getFirstElementChild(container);
    var thumbMargins = goog.style.getMarginBox(thumb);
    var thumbBorderBoxSize = goog.style.getBorderBoxSize(thumb);
    var thumbWidth = thumbBorderBoxSize.width +
        thumbMargins.right + thumbMargins.left;
    var thumbHeight = thumbBorderBoxSize.height +
        thumbMargins.top + thumbMargins.bottom;
    this.thumbSize_ = [thumbWidth, thumbHeight];

    var width = containerSize.width - thumbWidth;
    var height = containerSize.height - thumbHeight;

    var limits;
    if (containerSize.width > containerSize.height) {
        this.direction_ = anvi.control.Slider.direction.HORIZONTAL;
        limits = new goog.math.Rect(0, 0, width, 0);
    } else {
        this.direction_ = anvi.control.Slider.direction.VERTICAL;
        limits = new goog.math.Rect(0, 0, 0, height);
    }
    this.dragger_.setLimits(limits);
    this.sliderInitialized_ = true;
};

/**
 * Update the slider element.
 * @param {ol.MapEvent} mapEvent Map event.
 * @this {ol.control.ZoomSlider}
 * @api
 */
anvi.control.Slider.render = function(mapEvent) {
    if (goog.isNull(mapEvent.frameState)) {
        return;
    }
    goog.asserts.assert(goog.isDefAndNotNull(mapEvent.frameState.viewState),
      'viewState should be defined');
    if (!this.sliderInitialized_) {
        this.initSlider_();
    }
    var res = 0;
    if (res !== this.currentValue_) {
        this.currentValue_ = res;
        this.setThumbPosition_(res);
    }
};