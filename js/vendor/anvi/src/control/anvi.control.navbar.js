goog.provide('anvi.control.Navbar');

goog.require('anvi.control.NavbarItem');
goog.require('goog.style');

/**
 * @classdesc
 * Control for rendering navigation bar
 *
 * @constructor
 * @extends {ol.control.Control}
 * @param {Object=} opt_options Control options.
 * @api stable
 */
anvi.control.Navbar = function(opt_options) {

    var options = opt_options || {};
    var boardElm = options.boardHTML || "";
    var _left = options.left || undefined;
    //var _right = options.right || undefined;
    var _top = options.top || undefined;
    //var _bottom = options.bottom || undefined;

    this._eTarget = new goog.events.EventTarget();

    var this_ = this;

    this.navbar = document.createElement('ul');

    var element = document.createElement('div');
    element.className = 'anvi-control-navbar ol-unselectable ol-control';
    element.appendChild(this.navbar);
    goog.style.setPosition(element, _left || '3.0em', _top || '0.55em');

    ol.control.Control.call(this, {
        element: element,
        target: options.target
    });

};
ol.inherits(anvi.control.Navbar, ol.control.Control);

/**
 * Add an item into the navigation bar
 *
 * @param {Object=} opt_options Item options.
 * @api stable
 */
anvi.control.Navbar.prototype.addItem = function (opt_options) {
    var newItem = new anvi.control.NavbarItem(opt_options, this.navbar, this._eTarget);
    return newItem;//this.navbar_item;
}
