
goog.provide('anvi.map');

anvi.map = new ol.Map({
	view: new ol.View({
    	center: [0, 0],
    	zoom: 2
  	}),
  	layers: [
    
      new ol.layer.Tile({
        source: new ol.source.MapQuest({layer: 'sat'})
      })
    	/*
      new ol.layer.Tile({
      		source: new ol.source.OSM()
    	})*/
  	],
  	target: 'map'
});
