goog.provide('anvi.gis.Measure');

goog.require('ol.layer.Vector');
/**
 * @classdesc
 * Add an interaction for measuring
 *
 * @constructor
 * @param {olx.layer.VectorOptions=} opt_options Options.
 */
anvi.gis.Measure = function(opt_options) {

  var options = opt_options ? opt_options : {};

  var baseOptions = goog.object.clone(options);

  // Delete the options that do not belong to the parent class
  delete baseOptions.type;

  // Source
  baseOptions.source = new ol.source.Vector();

  // Style
  baseOptions.style = new ol.style.Style({
    fill: new ol.style.Fill({
      color: 'rgba(255, 255, 255, 0.2)'
    }),
    stroke: new ol.style.Stroke({
      color: '#ffcc33',
      width: 2
    }),
    image: new ol.style.Circle({
      radius: 7,
      fill: new ol.style.Fill({
        color: '#ffcc33'
      })
    })
  });

  /**
   * Constructor
   */
  goog.base(this, /** @type {olx.layer.VectorOptions} */ (baseOptions));

  /**
   * @private
   * @type {string|undefined}
   */
  this.type_ = opt_options.type || "area";

  this.wgs84Sphere_ = new ol.Sphere(6378137);

  /**
   * Currently drawn feature.
   * @type {ol.Feature}
   */
  this.sketch;


  /**
   * The help tooltip element.
   * @type {Element}
   */
  var helpTooltipElement;


  /**
   * Overlay to show the help messages.
   * @type {ol.Overlay}
   */
  var helpTooltip;


  /**
   * The measure tooltip element.
   * @type {Element}
   */
  this.measureTooltipElement;


  /**
   * Overlay to show the measurement.
   * @type {ol.Overlay}
   */
  this.measureTooltip;


  /**
   * Message to show when the user is drawing a polygon.
   * @type {string}
   */
  this.continuePolygonMsg = 'Click to continue drawing the polygon';


  /**
   * Message to show when the user is drawing a line.
   * @type {string}
   */
  this.continueLineMsg = 'Click to continue drawing the line';

  this.geodesic_ = true;

  this.draw_; // global so we can remove it later

  /* On mouse out
  document.getElementById(this.map_.getViewport()).on('mouseout', function() {
    document.getElementById(helpTooltipElement).addClass('hidden');
  });//*/

  


  /**
   * Format length output.
   * @param {ol.geom.LineString} line The line.
   * @return {string} The formatted length.
   */
  var formatLength = function(line) {
    var length;
    if (geodesicCheckbox.checked) {
      var coordinates = line.getCoordinates();
      length = 0;
      var sourceProj = this.map_.getView().getProjection();
      for (var i = 0, ii = coordinates.length - 1; i < ii; ++i) {
        var c1 = ol.proj.transform(coordinates[i], sourceProj, 'EPSG:4326');
        var c2 = ol.proj.transform(coordinates[i + 1], sourceProj, 'EPSG:4326');
        length += this.wgs84Sphere_.haversineDistance(c1, c2);
      }
    } else {
      length = Math.round(line.getLength() * 100) / 100;
    }
    var output;
    if (length > 100) {
      output = (Math.round(length / 1000 * 100) / 100) +
          ' ' + 'km';
    } else {
      output = (Math.round(length * 100) / 100) +
          ' ' + 'm';
    }
    return output;
  };


  /**
   * Format area output.
   * @param {ol.geom.Polygon} polygon The polygon.
   * @return {string} Formatted area.
   */
  var formatArea = function(polygon) {
    var area;
    if (this.geodesic_) {
      var sourceProj = this.map_.getView().getProjection();
      var geom = /** @type {ol.geom.Polygon} */(polygon.clone().transform(
          sourceProj, 'EPSG:4326'));
      var coordinates = geom.getLinearRing(0).getCoordinates();
      area = Math.abs(this.wgs84Sphere_.geodesicArea(coordinates));
    } else {
      area = polygon.getArea();
    }
    var output;
    if (area > 10000) {
      output = (Math.round(area / 1000000 * 100) / 100) +
          ' ' + 'km<sup>2</sup>';
    } else {
      output = (Math.round(area * 100) / 100) +
          ' ' + 'm<sup>2</sup>';
    }
    return output;
  };

  
  /**
   * Let user change the geometry type.
   *
  typeSelect.onchange = function() {
    map.removeInteraction(draw);
    addInteraction();
  };//*/

  this.helpTooltipElement = helpTooltipElement;
  this.helpTooltip = helpTooltip;
};
goog.inherits(anvi.gis.Measure, ol.layer.Vector);

/**
   * Handle pointer move.
   * @param {ol.MapBrowserEvent} evt The event.
   */
anvi.gis.Measure.prototype.pointerMoveHandler_ = function(evt) {
  if (evt.dragging) {
    return;
  }
  
  //this.createMeasureTooltip_();
  this.createHelpTooltip_();

  /** @type {string} */
  var helpMsg = 'Click to start drawing';

  if (this.sketch) {
    var geom = (this.sketch.getGeometry());
    if (geom instanceof ol.geom.Polygon) {
      helpMsg = this.continuePolygonMsg;
    } else if (geom instanceof ol.geom.LineString) {
      helpMsg = this.continueLineMsg;
    }
  }

  this.helpTooltipElement.innerHTML = helpMsg;
  this.helpTooltip.setPosition(evt.coordinate);

  this.helpTooltipElement.removeClass('hidden');
};

/**
 * Creates a new help tooltip
 */
anvi.gis.Measure.prototype.createHelpTooltip_ = function () {
  var helpTooltipElement = this.helpTooltipElement;
  if (this.helpTooltipElement) {
    console.log(this.helpTooltipElement.parentNode);
    if (this.helpTooltipElement.parentNode) {
      this.helpTooltipElement.parentNode.removeChild(this.helpTooltipElement);
    }
  }
  this.helpTooltipElement = document.createElement('div');
  this.helpTooltipElement.className = 'tooltip hidden';
  this.helpTooltip = new ol.Overlay({
    element: helpTooltipElement,
    offset: [15, 0],
    positioning: 'center-left'
  });

  if(this.map_) {
    this.map_.addOverlay(this.helpTooltip);
  }
}

/**
 * Creates a new measure tooltip
 */
anvi.gis.Measure.prototype.createMeasureTooltip_ = function () {
  var measureTooltipElement = this.measureTooltipElement_;
  if (measureTooltipElement) {
    measureTooltipElement.parentNode.removeChild(measureTooltipElement);
  }
  measureTooltipElement = document.createElement('div');
  measureTooltipElement.className = 'tooltip tooltip-measure';
  this.measureTooltip_ = new ol.Overlay({
    element: measureTooltipElement,
    offset: [0, -15],
    positioning: 'bottom-center'
  });

  if(this.map_) {
    this.map_.addOverlay(this.measureTooltip_);
  }
}

anvi.gis.Measure.prototype.addInteraction = function () {
  
  //
  var type = (this.type_ == 'area' ? 'Polygon' : 'LineString');
  this.draw_ = new ol.interaction.Draw({
    source: this.source_,
    type: /** @type {ol.geom.GeometryType} */ (type),
    style: new ol.style.Style({
      fill: new ol.style.Fill({
        color: 'rgba(255, 255, 255, 0.2)'
      }),
      stroke: new ol.style.Stroke({
        color: 'rgba(0, 0, 0, 0.5)',
        lineDash: [10, 10],
        width: 2
      }),
      image: new ol.style.Circle({
        radius: 5,
        stroke: new ol.style.Stroke({
          color: 'rgba(0, 0, 0, 0.7)'
        }),
        fill: new ol.style.Fill({
          color: 'rgba(255, 255, 255, 0.2)'
        })
      })
    })
  });
  this.map_.addInteraction(this.draw_);

  this.createMeasureTooltip_();
  this.createHelpTooltip_();

  this.draw_.on('drawstart',
    function(evt) {
      // set sketch
      this.sketch_ = evt.feature;

      /** @type {ol.Coordinate|undefined} */
      var tooltipCoord = evt.coordinate;

      this.listener_ = this.sketch_.getGeometry().on('change', function(evt) {
        var geom = evt.target;
        var output;
        if (geom instanceof ol.geom.Polygon) {
          output = formatArea(geom);
          tooltipCoord = geom.getInteriorPoint().getCoordinates();
        } else if (geom instanceof ol.geom.LineString) {
          output = formatLength(geom);
          tooltipCoord = geom.getLastCoordinate();
        }
        measureTooltipElement.innerHTML = output;
        measureTooltip.setPosition(tooltipCoord);
      });
    },
  this);

  this.draw_.on('drawend',
    function() {
      this.measureTooltipElement_.className = 'tooltip tooltip-static';
      this.measureTooltip_.setOffset([0, -7]);
      // unset sketch
      this.sketch_ = null;
      // unset tooltip so that a new one can be created
      this.measureTooltipElement_ = null;
      this.createMeasureTooltip_();
      ol.Observable.unByKey(this.listener_);
    },
  this);
}