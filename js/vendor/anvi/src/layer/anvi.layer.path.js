goog.provide('anvi.layer.Path');

/**
 * @classdesc
 * Layer for rendering the missile path points
 *
 * @constructor
 * @extends {ol.layer.Vector}
 * @param {olx.layer.IsocontourOptions=} opt_options Options.
 * @api stable
 */
anvi.layer.Path = function(opt_options) {

    var options = opt_options ? opt_options : {};

    var baseOptions = goog.object.clone(options);

    // Delete the options that do not belong to the parent class
    delete baseOptions.url;
    delete baseOptions.stats;

    // Add new one
    baseOptions.source = new ol.source.Vector();

    /**
     * Constructor
     */
    goog.base(this, /** @type {olx.layer.VectorOptions} */ (baseOptions));

    
    /**
     * @private
     * @type {string|undefined}
     */
    this.url_ = options.url || "";

    /**
     * @private
     * @type {Array.<number>|undefined}
     */
    this.stats = options.stats || undefined;

    /**
     * @private
     * @type {Array.<ol.Feature>}
     */
    this.feats_ = [];

    //this.dataLoaded = new goog.event.Event('dataLoaded', { isDecaf: isDecaf });
    //goog.events.dispatchEvent(this.dataLoaded);

    // Get the source data from url
    if(this.url_) {
        // Get the source format
        //var format = this.url_.match(/\.\w+/i)[0];
        var this_ = this;
        var format = ".wkt";

        if(format.length>0) {
            // Call xhr to get the data

            goog.net.XhrIo.send(
                this.url_,
                function(e){
                    var xhr = e.target;

                    if(xhr.getLastErrorCode() != goog.net.ErrorCode.NO_ERROR) {
                        document.getElementById("loading").style.display = "none";
                        swal("Cannot get the path data...", xhr.getLastError(), "error");
                        throw xhr.getLastError();
                    }

                    // Get the response object
                    try {
                        var _data = xhr.getResponseJson();

                        if(this_.stats == undefined) {
                            this_.stats = _data.stats || undefined;
                        }

                        if(format.toLowerCase() === ".geojson") {
                            this_.feats_ = (new ol.format.GeoJSON()).readFeatures(
                                _countours,
                                {
                                    dataProjection :'EPSG:4326', 
                                    featureProjection: 'EPSG:3857'
                                }
                            );

                        } else if (format.toLowerCase() === ".wkt") {
                            var wkt_ = new ol.format.WKT();
                            console.log('DB loaded');
                            this_.feats_ = wkt_.readFeatures(_data.wkt);
                            for (var i = 0; i < this_.feats_.length; i++) {
                                //feat.setProperties({'value': _countours[i].value || null});
                                //feat.setProperties({'color': _countours[i].color || null});
                                this_.feats_[i].getGeometry().transform('EPSG:4326', 'EPSG:3857');
                            };
                        }

                        // Add features to the source
                        this_.getSource().addFeatures(this_.feats_);

                        // Dispatch event
                        //this_.change('extent');
                    } catch (err) {
                        throw err;
                    }
                },
                "GET"
            );

            
        }
    };//*/


    /**
     * Define the style for the wind flow layer
     */
    this.setStyle ( goog.bind( function(feature, resolution) {
        var text;
        var icon_src = "", angle = 0.;
            offsetY = 10;
        var props = feature.getProperties();

        //var style = styleCache[size];
        //if (!style) {
        style = [
            new ol.style.Style({
                image: new ol.style.Circle({
                    radius: 2,
                    stroke: new ol.style.Stroke({
                        color: 'red'//#333'
                    }),
                    fill: new ol.style.Fill({
                        color: 'red'//'#333'
                    })
                })
            })
        ];
        //}

        return style;

    }, this ));//*/

};
goog.inherits(anvi.layer.Path, ol.layer.Vector);

