goog.provide('anvi.field.Wind');

/**
 * @classdesc
 * Layer for rendering wind field vector data as flow lines
 *
 * @constructor
 * @extends {ol.layer.Image}
 * @param {olx.layer.IsocontourOptions=} opt_options Options.
 * @api stable
 */
anvi.field.Wind = function(opt_options) {

    var options = opt_options ? opt_options : {};

    var baseOptions = goog.object.clone(options);

    // Delete the options that do not belong to the parent class
    delete baseOptions.url;

    // Add new one
    baseOptions.source = new ol.source.ImageVector({
        source: new ol.source.Vector()
    });

    /**
     * Constructor
     */
    goog.base(this, /** @type {olx.layer.VectorOptions} */ (baseOptions));

    /**
     * @private
     * @type {string|undefined}
     */
    this.url_ = options.url || "";

    /**
     * @private
     * @type {Array.<ol.Feature>}
     */
    this.feats_ = [];

    // Get the source data from url
    if(this.url_) {
        // Get the source format, by default geojson
        var format = this.url_.match(/\.\w+/i);
        if(format) {
            format = format[0];
        } else {
            format = '.geojson';
        }

        var this_ = this;
        if(format.length>0) {
            // Call xhr to get the data

            goog.net.XhrIo.send(
                this.url_,
                function(e){
                    var xhr = e.target;

                    if(xhr.getLastErrorCode() != goog.net.ErrorCode.NO_ERROR) {
                        document.getElementById("loading").style.display = "none";
                        swal("Cannot get the wind data...", xhr.getLastError(), "error");
                        throw xhr.getLastError();
                    }

                    // Get the response object
                    try {
                        var _data = xhr.getResponseJson();

                        if (format.toLowerCase() === ".wkt") {
                            /*
                            var wkt_ = new ol.format.WKT();
                            for (var i = 0; i < _countours.length; i++) {
                                var feat = wkt_.readFeature(_countours[i].wkt);
                                feat.setProperties({'value': _countours[i].value || null});
                                feat.setProperties({'color': _countours[i].color || null});
                                feat.getGeometry().transform('EPSG:4326', 'EPSG:3857');
                                this_.feats_.push(feat);
                            };*/
                        } else if (format.toLowerCase() === ".geojson") {
                            console.log("DB loaded!");
                            // GeoJson by default
                            this_.feats_ = (new ol.format.GeoJSON()).readFeatures(
                                _data,
                                {
                                    dataProjection :'EPSG:4326', 
                                    featureProjection: 'EPSG:3857'
                                }
                            );
                        }

                        // Add features to the source
                        this_.getSource().getSource().addFeatures(this_.feats_);

                        // Dispatch event
                        //this_.change('extent');
                    } catch (err) {
                        throw err;
                    }
                },
                "GET"
            );

            
        }
    };//*/


    /**
     * Define the style for the wind flow layer
     */
    this.getSource().setStyle ( goog.bind( function(feature, resolution) {

        var text;
        var icon_src = "", angle = 0.;
            offsetY = 10;
        var props = feature.getProperties();

        // Wind
        if(props.modulus && props.direction) {
            var ws_ = props.modulus*3.6;
            var iconId = 0;
            if(ws_ < 1) {
                iconId = 0;
            } else if(ws_ >= 1 && ws_ < 6) {
                iconId = 1;
            } else if(ws_ >= 6 && ws_ < 12) {
                iconId = 2;
            } else if(ws_ >= 12 && ws_ < 20) {
                iconId = 3;
            } else if(ws_ >= 20 && ws_ < 29) {
                iconId = 4;
            } else if(ws_ >= 29 && ws_ < 39) {
                iconId = 5;
            } else if(ws_ >= 39 && ws_ < 50) {
                iconId = 6;
            } else if(ws_ >= 50 && ws_ < 62) {
                iconId = 7;
            } else if(ws_ >= 62 && ws_ < 75) {
                iconId = 8;
            } else if(ws_ >= 75 && ws_ < 89) {
                iconId = 9;
            } else if(ws_ >= 89 && ws_ < 103) {
                iconId = 10;
            } else if(ws_ >= 103 && ws_ < 118) {
                iconId = 11;
            } else if(ws_ >= 118) {
                iconId = 12;
            }

            icon_src = anvi.root + "src/img/145px-Logo_beaufort" + iconId + ".png";
            angle = -Math.PI*0.5 + Math.PI*props.direction/180;
        }
        //ws = ws/ifeat;
        //ws = ws.toFixed(0);

        //var style = styleCache[size];
        //if (!style) {
        style = [
            new ol.style.Style({
                image: new ol.style.Icon({
                    anchor: [1., 0.57],
                    anchorXUnits: 'fraction',
                    anchorYUnits: 'fraction',
                    opacity: 0.75,
                    scale: 0.2,
                    rotation: angle,
                    src: icon_src
                })
            }),
            new ol.style.Style({
                image: new ol.style.Circle({
                    radius: 2,
                    stroke: new ol.style.Stroke({
                        color: 'red'//#333'
                    }),
                    fill: new ol.style.Fill({
                        color: 'red'//'#333'
                    })
                })
            })
        ];
        //}

        return style;

    }, this ));//*/

};
goog.inherits(anvi.field.Wind, ol.layer.Image);
