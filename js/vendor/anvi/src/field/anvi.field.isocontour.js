goog.provide('anvi.field.Isocontour');

//goog.require('goog.events.Event');
/**
 * @classdesc
 * Layer for rendering the concentration data field
 *
 * @constructor
 * @extends {ol.layer.Image}
 * @param {olx.layer.IsocontourOptions=} opt_options Options.
 * @api stable
 */
anvi.field.Isocontour = function(opt_options) {

    var options = opt_options ? opt_options : {};

    var baseOptions = goog.object.clone(options);

    // Delete the options that do not belong to the parent class
    delete baseOptions.url;
    delete baseOptions.stats;

    // Add new one
    baseOptions.source = new ol.source.ImageVector({
        source: new ol.source.Vector()
    });

    /**
     * Constructor
     */
    goog.base(this, /** @type {olx.layer.VectorOptions} */ (baseOptions));

    
    /**
     * @private
     * @type {string|undefined}
     */
    this.url_ = options.url || "";

    /**
     * @private
     * @type {Array.<number>|undefined}
     */
    this.stats = options.stats || undefined;

    /**
     * @private
     * @type {Array.<ol.Feature>}
     */
    this.feats_ = [];

    //this.dataLoaded = new goog.event.Event('dataLoaded', { isDecaf: isDecaf });
    //goog.events.dispatchEvent(this.dataLoaded);

    // Get the source data from url
    if(this.url_) {
        // Get the source format
        //var format = this.url_.match(/\.\w+/i)[0];
        var this_ = this;
        var format = ".geojson";

        if(format.length>0) {
            // Call xhr to get the data

            goog.net.XhrIo.send(
                this.url_,
                function(e){
                    var xhr = e.target;

                    if(xhr.getLastErrorCode() != goog.net.ErrorCode.NO_ERROR) {
                        document.getElementById("loading").style.display = "none";
                        swal("Cannot get the countour data...", xhr.getLastError(), "error");
                        throw xhr.getLastError();
                    }

                    // Get the response object
                    try {
                        var _data = xhr.getResponseJson();
                        var _countours = _data.contour;

                        if(this_.stats == undefined) {
                            this_.stats = _data.stats || undefined;
                        }

                        if(format.toLowerCase() === ".geojson") {
                            this_.feats_ = (new ol.format.GeoJSON()).readFeatures(
                                _countours,
                                {
                                    dataProjection :'EPSG:4326', 
                                    featureProjection: 'EPSG:3857'
                                }
                            );

                        } else if (format.toLowerCase() === ".wkt") {
                            var wkt_ = new ol.format.WKT();

                            for (var i = 0; i < _countours.length; i++) {
                                var feat = wkt_.readFeature(_countours[i].wkt);
                                feat.setProperties({'value': _countours[i].value || null});
                                feat.setProperties({'color': _countours[i].color || null});
                                feat.getGeometry().transform('EPSG:4326', 'EPSG:3857');
                                feat.setStyle ( getStyleFunction_ );
                                this_.feats_.push(feat);
                            };
                        }

                        // Add features to the source
                        this_.getSource().getSource().addFeatures(this_.feats_);

                        // Dispatch event
                        //this_.change('extent');
                    } catch (err) {
                        throw err;
                    }
                },
                "GET"
            );

            
        }
    };//*/

    /**
     * Define the style for the wind flow layer
     */
    this.getSource().setStyle ( goog.bind( function(feature, resolution) {
        var clr = 'gray';
        var val = feature.get('value').toFixed(0);

        /*
        if(clr = feature.get('color')) {
            //console.log(clr);
        } else {
            val = feature.get('value').toFixed(1);
            var clr = 'black';
            clr = this.colorSchemeFunction_(val);

            // Save the color property
            //feature.setProperties({'color': clr});
        }*/

        // Save style
        //feature.setStyle(style);

        return [
            new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: clr,
                    width: 1
                })
            }),
            new ol.style.Style({
                text: new ol.style.Text({
                    text: val ? val.toString() : 'NaN',
                    offsetX: 0,
                    fill: new ol.style.Fill({
                        color: clr
                    }),
                    stroke: new ol.style.Stroke({
                        color: clr,
                        width: 1
                    }),
                    textBaseline: "Middle"
                })
            }),
        ];
    }, this ));//*/

};
goog.inherits(anvi.field.Isocontour, ol.layer.Image);

/**
 * Get the color from palette function
 * @private
 * @type {function}
 */
anvi.field.Isocontour.prototype.colorSchemeFunction_ = function (value) {
    var min = this.stats.min || 0., max = this.stats.max || 1.;
    var clr = 'black';
    var palette = ["green", "cyan", "orange"];

    if(min>max) return false;
    if( value <= min ) return "blue";
    if( value > max ) return "red"; 

    var step = Math.floor((max-min)/palette.length);
    var clrId = Math.floor( (value-min)/step );
    clr = palette[clrId];

    return clr;
};//*/
