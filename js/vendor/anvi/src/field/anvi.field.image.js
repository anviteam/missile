goog.provide('anvi.field.Image');

/**
 * @classdesc
 * Layer for rendering wind field vector data as flow lines
 *
 * @constructor
 * @extends {ol.layer.Image}
 * @param {olx.layer.VectorOptions=} opt_options Options.
 * @api stable
 */
anvi.field.Image = function(options) {

    var baseOptions = goog.object.clone(options);

    // Delete the options that do not belong to the parent class
    delete baseOptions.url;

    /**
     * Constructor
     */
    goog.base(this, /** @type {olx.layer.VectorOptions} */ (baseOptions));

    /**
     * @private
     * @type {string|undefined}
     */
    this.url_ = options.url || "";


    // Get the source data from url
    if(this.url_) {
        
        var this_ = this;
        // Call xhr to get the data

        goog.net.XhrIo.send(
            this.url_,
            function(e){
                var xhr = e.target;

                if(xhr.getLastErrorCode() != goog.net.ErrorCode.NO_ERROR) {
                    document.getElementById("loading").style.display = "none";
                    swal("Cannot get the image data...", xhr.getLastError(), "error");
                    throw xhr.getLastError();
                }

                // Get the response object
                try {
                    var _data = xhr.getResponseJson();

                    // Set the source properties
                    this_.setSource ( 
                    	new ol.source.ImageStatic({
					        attributions: [
						      	new ol.Attribution({
						        	html: '&copy; <a href="http://www.anvitechnology.io">AnviTechnology</a>'
						      	})
						    ],
						    'url': _data.imgUrl,
						    'projection': _data.meta.projection,
						    'imageExtent': _data.meta.extent
					    })
				    );

                    // Save url
                    this_.getSource().set('lon', 0.5*(_data.meta.extent[0] + _data.meta.extent[2]));
                    this_.getSource().set('lat', 0.5*(_data.meta.extent[1] + _data.meta.extent[3]));
                    this_.getSource().setProperties({
                        'url': _data.imgUrl,
                        'extent': _data.meta.extent});
                    //this_.dispatchEvent('change:source');
                } catch (err) {
                    document.getElementById("loading").style.display = "none";
                    swal("Error while creating the image layer...", err, "error");
                    throw err;
                }
            },
            "GET"
        );

    }//*/

};
goog.inherits(anvi.field.Image, ol.layer.Image);

