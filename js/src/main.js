/**
 * @copyright AnVi 2015
 */
goog.require('goog.dom');

goog.require('goog.net.XhrIo');

goog.require('anvi.control.Navbar');
goog.require('anvi.control.Slider');
goog.require('anvi.control.Select');

goog.require('anvi.gis.Measure');


/**
 * Access DB for quering data layer, added to map
 */
function loadLayersFromDB () {

}

/////////////////////////////////////////////////
function init () {
/////////////////////////////////////////////////

// Set target to the map
// ==========================
//anvi.map.setTarget('map');

// Remove Attribution control
//anvi.map.removeControl();

// Add interactions
anvi.map.addInteraction(anvi.map.dragAndDropInteraction);
anvi.map.dragAndDropInteraction.on('addfeatures', function(event) {

  /*
  var reader = new FileReader();

  reader.onload = function(event) {
    var dataURL = event.target.result;console.log(dataURL);
    var mimeType = dataURL.split(",")[0].split(":")[1].split(";")[0];
    console.log(mimeType);
  };

  reader.readAsDataURL(event.file);//*/

  // Get the file extension
  var _fext = event.file.type;
  var _fname = event.file.name;

  if(!_fext) {
    _fext = _fname.match(/\.\w+$/);
  }

  // Binary file (radar, satellite)
  if(/radar/i.test(_fext)) {
    // Radar
    binaryFileSender({
      file: event.file,
      type: 'radar'
    });
  } else if(/awx/i.test(_fext)) {
    binaryFileSender({
      file: event.file,
      type: 'sattelite'
    });
  } else {
    try {
      textFileSender({
        file: event.file
      });
    } catch (err) {
      // Show dialog for choose the file format
      throw new 'No acceptable file extension found! [.radar | .awx]';
    }
  }
  //*/
});

/////////////////////////////////////////////////
// Global variables
/////////////////////////////////////////////////

/////////////////////////////////////////////////
// UIs
/////////////////////////////////////////////////
// Const
anvi.dataUrl = window.location.origin + "/missile/api/data/";
var currentTime = "";
var currentPressure = 9999;

// Select
// --------------------------
var timeSelector = new anvi.control.Select({
  position: 'bottomright',
  baseUrl: anvi.dataUrl
});
timeSelector.reload();
anvi.map.addControl(timeSelector);

// Sliders
// --------------------------
// Altitude slider
var altiSlider = new anvi.control.Slider({
  position: 'right',
  min: 0,
  max: 19,
  orient: 'vertical',
  interval: 1,
  legend: 'hPa',
  labels: ['surface','1000','950','900','850','800','750','700','650','600','550','500','450','400','350','300','250','200','150','100']
});
anvi.map.addControl(altiSlider);
// Time frame slider
var timeSlider = new anvi.control.Slider({
  position: 'bottom',
  min: 0,
  max: 10,
  orient: 'horizontal',
  legend: 'Play',
  legendOnclick: function () {
    alert('Run animation .... in progress.');
  },
  bubble: false
});
anvi.map.addControl(timeSlider);

// Create the toolbars
// --------------------------
var GISToolbar = new anvi.control.Navbar();
GISToolbar.addItem({
  name: 'Area',
  active: true,
  onClick: function (tooggle) {
    // Pointer on move
    anvi.helpers.activateGIS({type: 'area', activated: tooggle});
  }
});
//GISToolbar.addItem({name: 'M'});
GISToolbar.addItem({
  name: 'Dist',
  active: true,
  onClick: function (tooggle) {
    // Pointer on move
    anvi.helpers.activateGIS({type: 'distance', activated: tooggle});
  }
});
GISToolbar.addItem({
  name: 'Clear',
  onClick: function () {
    // Remove interaction
    if (anvi.helpers.draw) {
      anvi.map.removeInteraction(anvi.helpers.draw);
    }

    // Remove GIS layer
    if (anvi.helpers.drawingLayer) {
      anvi.helpers.drawingLayer.getSource().clear();
    }

    // Remove tooltips
    var overlays = anvi.map.getOverlays();
    if (overlays) {
      overlays.clear();
    }

    // Unset event pointermove
    if (anvi.helpers.pointermoveListener) {
      anvi.map.unByKey(anvi.helpers.pointermoveListener);
      anvi.helpers.pointermoveListener = null;
    }
  }
});
anvi.map.addControl(GISToolbar);

// Main navigation bar
/////////////////////////////////////////////////
var mainToolbar = new anvi.control.Navbar({
  left: '13.0em'
});

// Observation
// --------------------------
var obsItem = mainToolbar.addItem({
  name: 'Observation'
});
// Temperature
mainToolbar.addItem({
  name: 'Temperature',
  parent: obsItem,
  onClick: function () {
    showData(anvi.dataUrl, 'temperature', 'observation', 'isocontour');
  }
});
mainToolbar.addItem({
  name: 'Height',
  parent: obsItem,
  onClick: function () {
    showData(anvi.dataUrl, 'height', 'observation', 'isocontour');
  }
});
mainToolbar.addItem({
  name: 'Rain',
  parent: obsItem,
  onClick: function () {
    showData(anvi.dataUrl, 'rain', 'plot', 'plot');
  }
});
mainToolbar.addItem({
  name: 'UV wind',
  parent: obsItem,
  onClick: function () {
    showData(anvi.dataUrl, 'wind', 'observation', 'wind');
  }
});


// Radar
// --------------------------
var radarItem = mainToolbar.addItem({
  name: 'Radar'
});
mainToolbar.addItem({
  name: 'Radar basic',
  parent: radarItem,
  onClick: function () {
    showData(anvi.dataUrl, null, 'radar', 'image');
  }
});
mainToolbar.addItem({
  name: 'Radar 19',
  parent: radarItem,
  onClick: function () {
    showData(anvi.dataUrl, null, 'radar', 'image');
  }
});

// Satellite
// --------------------------
var satItem = mainToolbar.addItem({
  name: 'Satelitte'
});
mainToolbar.addItem({
  name: 'Satelitte IR1',
  parent: satItem,
  onClick: function () {
    showData(anvi.dataUrl, null, 'satellite', 'image');
  }
});
mainToolbar.addItem({
  name: 'Satelitte TBB',
  parent: satItem,
  onClick: function () {
    showData(anvi.dataUrl, null, 'satellite', 'image');
  }
});

// EC
// --------------------------
var ecItem = mainToolbar.addItem({
  name: 'EC'
});
// Height
mainToolbar.addItem({
  name: 'Height',
  parent: ecItem,
  onClick: function () {
    showData(anvi.dataUrl, 'height', 'ec', 'isocontour');
  }
});
// UV Wind
mainToolbar.addItem({
  name: 'UV Wind',
  parent: ecItem,
  onClick: function () {
    showData(anvi.dataUrl, 'wind', 'ec', 'wind');
  }
});
// UV thin
mainToolbar.addItem({
  name: 'UV thin',
  parent: ecItem,
  onClick: function () {
    showData(anvi.dataUrl, 'thin', 'ec', 'wind');
  }
});


/* Cloud
mainToolbar.addItem({
  name: 'Cloud',
  type: 'cloud'
});*/

anvi.map.addControl(mainToolbar);

// IO toolbar
// --------------------------
var IOToolbar = new anvi.control.Navbar({
  left: '34.0em'
});
IOToolbar.addItem({
  name: 'Import',
  onClick: function () {
    document.getElementById('import_widget').style.display="block";
  }
});
IOToolbar.addItem({
  name: 'Export',
/*
  onClick: function () {
    document.getElementById('export_widget').style.display="block";
  },*/
  onClick: function () {
    // Get all visible layers
    var layers = anvi.map.getLayers();
    layers.forEach( function (lay, id) {
      if ( lay.getVisible() ) {
        var src = lay.getSource();
        var laytyp = lay.get('type');
        var feats = [];
        var kml = new ol.format.KML();
        var kmlstr;

        if (laytyp == "image") {
          var url_ = src.get('url');
          var lon_ = src.get('lon');
          var lat_ = src.get('lat');
          var props_ = src.getProperties();
          var projExtent = ol.proj.transformExtent(props_.extent, 'EPSG:3857', 'EPSG:4326');
          var alti_ = 2744.0;

          // Save to kmz
          var xw = new XMLWriter('UTF-8');
          xw.formatting = 'none';//add indentation and newlines indented/none
          xw.indentChar = ' ';//indent with spaces
          xw.indentation = 2;//add 2 spaces per level

          xw.writeStartDocument( );
          xw.writeStartElement( 'kml' );
          xw.writeAttributeString('xmlns','http://www.opengis.net/kml/2.2');
          xw.writeStartElement( 'Folder' );

            xw.writeElementString('name', lay.get('name'));
            xw.writeElementString('description', 'Radar data on the ground');

            xw.writeStartElement('GroundOverlay');
              xw.writeElementString('name', 'radar');
              xw.writeElementString('description', 'Radar data on the ground');

              xw.writeStartElement( 'Icon');
                xw.writeElementString('href', props_.url);
              xw.writeEndElement();
              xw.writeElementString('altitude', alti_.toString());
              xw.writeElementString('altitudeMode', 'absolute');
              xw.writeStartElement( 'LatLonBox');
                xw.writeElementString('north', projExtent[3].toString());
                xw.writeElementString('south', projExtent[1].toString());
                xw.writeElementString('east', projExtent[2].toString());
                xw.writeElementString('west', projExtent[0].toString());
                xw.writeElementString('rotation', '0');
              xw.writeEndElement();
            xw.writeEndElement();

          xw.writeEndElement();
          xw.writeEndElement();
          xw.writeEndDocument();

          kmlstr = xw.flush();
          //console.log(kmlstr);          

          //kmlstr = new XMLSerializer().serializeToString(kml_);
          var blob_ = new Blob([kmlstr], {type : 'application/vnd.google-earth.kml+xml'}); // the blob
          //window.open(URL.createObjectURL(blob_));
          saveAs(blob_, "save.kmz");

        } else if (laytyp == "isocontour") {
          var vecsrc = lay.getSource().getSource();
          feats = vecsrc.getFeatures();

          // Save to kmz
          kmlstr = kml.writeFeatures(feats, {
            dataProjection: 'EPSG:4326',
            featureProjection: 'EPSG:3857'
          });

          var blob_ = new Blob([kmlstr], {type : 'application/vnd.google-earth.kml+xml'}); // the blob
          //window.open(URL.createObjectURL(blob_));
          saveAs(blob_, "save.kmz");

        }
        //var blob_ = new Blob([kmlstr], {type : 'application/vnd.google-earth.kml+xml'}); // the blob
        //saveAs(blob_, "test.kmz");
      }
    });
  }
});
IOToolbar.addItem({
  name: 'Run',
  onClick: function () {
    // Call server process
    goog.net.XhrIo.send(
      anvi.dataUrl + 'run',
      function(e){
          var xhr = e.target;

          if(xhr.getLastErrorCode() != goog.net.ErrorCode.NO_ERROR) {
              swal("API service has released an error...", xhr.getLastError(), "error");
          }

          // Get the response object
          try {
              var _data = xhr.getResponseJson();

              if (!_data.error) {
                swal({
                  title: "<h2>External program output</h2>",
                  text: _data.output,
                  html: true
                });
              } else {
                swal("Cannot run the external program...", "Error code: " + _data.error, "error");
              }
          } catch (err) {
              throw err;
          }
      },
      "GET"
    );
  }
});
IOToolbar.addItem({
  name: 'Results',
  onClick: function () {
    goog.net.XhrIo.send(
      anvi.dataUrl + 'result',
      function(e){
          var xhr = e.target;

          if(xhr.getLastErrorCode() != goog.net.ErrorCode.NO_ERROR) {
              swal("API service has released an error...", xhr.getLastError(), "error");
          }

          // Get the response object
          try {
              var _data = xhr.getResponseJson();

              if (!_data.error) {
                swal({
                  title: "<h2>Results</h2>",
                  text: _data.result,
                  html: true
                });
              } else {
                swal("Cannot get the result...", "Error code: " + _data.error, "error");
              }
          } catch (err) {
              throw err;
          }
      },
      "GET"
    );
  }
});
anvi.map.addControl(IOToolbar);


// Missile Menu
// --------------------------
var missileToolbar = new anvi.control.Navbar({
  top: '4.5em',
  left: '0.5em'
});
var missileMenu = missileToolbar.addItem({
  name: 'Missile'
});
anvi.map.addControl(missileToolbar);
missileToolbar.addItem({
  name: 'Missile traj 1',
  parent: missileMenu,
  type: 'path',
  datatype: 'missile',
  time: '2015-05-06 10:30:00',
  baseurl: anvi.dataUrl
});


//Mouse Position
// --------------------------
var mousePositionControl = new ol.control.MousePosition({
  coordinateFormat:ol.coordinate.createStringXY(4), //This is the format we want the coordinate in. 
  projection:"EPSG:4326", //This is the actual projection of the coordinates. 
  target:undefined, //define a target if you have a div you want to insert into already,
  undefinedHTML: '&nbsp;' //what openlayers will use if the map returns undefined for a map coordinate.
});
anvi.map.addControl(mousePositionControl);

/////////////////////////////////////////////////
// Map events
/////////////////////////////////////////////////
/*
anvi.map.on('click', function(evt) {
  var feature = anvi.map.forEachFeatureAtPixel(evt.pixel, function(feature, layer) {
    return feature;
  });
  if (feature) {
    var features = feature.get('features');
    var i;
    for (i = 0; i < features.length; ++i) {
      console.log(features[i].get('wind_dir'));
    }
  }
});//*/


/////////////////////////////////////////////////
// Widget events
/////////////////////////////////////////////////

document.getElementById("import_form").onsubmit = function (e) {
  e.preventDefault();

  var elms = this.elements;
  var fileInputElm = elms['uploadFileInput'];
  var fileTypeElm = elms['type_file_select'];

  if (fileInputElm.files.length>0) {
    for (var i=0; i<fileInputElm.files.length;i++) {
      anvi.helpers.sendDataFile({file: fileInputElm.files[i], type: fileTypeElm.value});
    }
    // Hide the widget
    document.getElementById('import_widget').style.display = 'none';
    // Notification
    swal("Files have been uploaded", "In case of error in server, it will be notified", "success");
  } else {
    swal('Oops...','No file selected','warning');
  }
}


/**
 * @param {string} type Data type.
 * @private
 */
function showData (baseurl, variable, datatype, geomtype) {

    var time = timeSelector.getCurrentValue();
    var pressure = altiSlider.value();
console.log(pressure);
    // Convert in to the surface pressure
    if (pressure === "surface") pressure = 9999;
    if (datatype === "radar" || datatype === "satellite") {
      pressure = undefined;
    }

    // Show loading icon
    var loadingElm = document.getElementById("loading");
    loadingElm.style.display = "block";

    // Add layer to map
    ///////////////////////////////////
    var url = baseurl + datatype;
    if (variable) {
        url += '/' + variable;
    }
    var id;
    if (time) {
      url += '/' + time;
      id += time;

        if (pressure) {
            url += '/' + pressure;
            id += '-' + pressure;
        }
    }
console.log('url: ' + url);

    // Plot url
    var enablePlot = true;
    if (datatype == "ec") {
      if (variable === "wind") {
        variable = "ecmwf_wind";
      } else if (variable === "height") {
        variable = "ecmwf_height";
      } else if (variable === "thin") {
        enablePlot = false;
      }
    }
    var urlPlot = baseurl + 'plot/' + variable;
    if (time) {
        urlPlot += '/' + time;

        if (pressure && (datatype !== "radar" || datatype !== "satellite")) {
            urlPlot += '/' + pressure;
        }
    }
console.log('geomtype: ' + geomtype);
    try {
        // Create a data layer based on type
        var dataLayer;
        if (geomtype === "isocontour") {
            dataLayer = new anvi.field.Isocontour({
                url: url,
                stats: this._stats
            });
        } else if (geomtype === "wind") {
            dataLayer = new anvi.field.Wind({
                url: url
            });
        } else if (geomtype === "path") {
            dataLayer = new anvi.layer.Path({
                url: url,
                stats: stats
            });
        } else if (geomtype === "image" || typeof geomtype == "undefined") {
          console.log('geomtype: ' + geomtype);
            dataLayer = new anvi.field.Image({
                url: url,
                opacity: 0.6
            });
        }
        dataLayer.set('type', geomtype || 'image');
        dataLayer.set('id', id || 'noid');
        // Add to map
        anvi.map.addLayer(dataLayer);

        // Show plot layer
        if (enablePlot && geomtype != 'image' && geomtype != 'wind' && geomtype != 'path') {
          console.log('urlPlot: ' + encodeURI(urlPlot));
          var plotLayer = new anvi.layer.Symbol({
              url: encodeURI(urlPlot)
          });
          plotLayer.set('id', id || 'noid');
          anvi.map.addLayer(plotLayer);
        }

        // Show only this layer
        anvi.helpers.showOnlyLayer(dataLayer);
    } catch (err) {
        swal("Oops...", err, "error");
        loadingElm.style.display = "none";
        throw err;
    }

    // Zoom on layer
    /*
    dataLayer.addEventListener('change:source', function () {
        loadingElm.style.display = "none";
        //anvi.map.getView().fit(weatherLayer.getSource().getSource().getExtent(), anvi.map.getSize());
    }, false);*/

    if(geomtype && (geomtype != "image" && geomtype != "path") ) {
        dataLayer.getSource().getSource().addEventListener('change', function () {
            loadingElm.style.display = "none";
            //anvi.map.getView().fit(weatherLayer.getSource().getSource().getExtent(), anvi.map.getSize());
        }, false);
    } else if (geomtype === "path") {
        loadingElm.style.display = "none";
    } else {
        dataLayer.addEventListener('change:source', function () {
            loadingElm.style.display = "none";
            //anvi.map.getView().fit(weatherLayer.getSource().getSource().getExtent(), anvi.map.getSize());
        }, false);
    }//*/
    
}

}
